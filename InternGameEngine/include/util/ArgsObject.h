#ifndef ARGS_OBJECT_H
#define ARGS_OBJECT_H

#include <unordered_map>

namespace util
{
	class ArgsObject
	{
		public:
			const int* getIntArg(std::string argName) const;
			void setIntArg(std::string argName, int intArg);
			const float* getFloatArg(std::string argName) const;
			void setFloatArg(std::string argName, float floatArg);
			const bool* getBoolArg(std::string argName) const;
			void setBoolArg(std::string argName, bool boolArg);
			const std::string* getStringArg(std::string argName) const;
			void setStringArg(std::string argName, std::string stringArg);

		private:
			std::unordered_map<std::string, int> m_intArgsMap;
			std::unordered_map<std::string, float> m_floatArgsMap;
			std::unordered_map<std::string, bool> m_boolArgsMap;
			std::unordered_map<std::string, std::string> m_stringArgsMap;
	};
}

#endif