#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H

#include <string>
#include <unordered_map>

namespace util
{
	template <typename ResourceType>
	class ResourceManager
	{
		public:
			void addResource(std::string resourceName, ResourceType resource);
			const ResourceType* getResource(std::string resourceName);
			bool isResourceLoaded(std::string resourcecName);
			void clear();

		private:
			std::unordered_map<std::string, std::unique_ptr<ResourceType>> m_resourceMap;
	};

	template<typename ResourceType>
	inline void ResourceManager<ResourceType>::addResource(std::string resourceName, ResourceType resource)
	{
		if (m_resourceMap.find(resourceName) == m_resourceMap.end())
		{
			m_resourceMap.insert({ resourceName, std::make_unique<ResourceType>(resource) });
		}
	}

	template<typename ResourceType>
	inline const ResourceType * ResourceManager<ResourceType>::getResource(std::string resourceName)
	{
		if (m_resourceMap.find(resourceName) != m_resourceMap.end())
		{
			return m_resourceMap[resourceName].get();
		}
		return nullptr;
	}

	template<typename ResourceType>
	inline bool ResourceManager<ResourceType>::isResourceLoaded(std::string resourcecName)
	{
		if (m_resourceMap.find(resourcecName) != m_resourceMap.end())
		{
			return true;
		}
		return false;
	}

	template<typename ResourceType>
	inline void ResourceManager<ResourceType>::clear()
	{
		m_resourceMap.clear();
	}
}

#endif