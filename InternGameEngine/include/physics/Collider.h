#ifndef COLLIDER_H
#define COLLIDER_H

#include <ecs/IComponent.h>
#include <physics/Collision.h>
#include <physics/BoundingBox.h>

#include <SFML/Graphics.hpp>
#include <vector>

namespace physics
{
	class Collider : public ecs::IComponent
	{
		public:
			Collider(std::string tagName);
			const std::string& getTagName();
			const std::vector<BoundingBox>& getBoundingBoxes();
			const std::vector<Collision>& getCollisions();
			void addBoundingBox(const BoundingBox& boundingBox);
			void setPosition(sf::Vector2f position);
			void addCollision(const Collision& collision);
			void checkCollided(Collider& otherCollider);
			void clearCollisions();

		private:
			std::string m_tagName;
			std::vector<BoundingBox> m_boundingBoxes;
			std::vector<Collision> m_collisions;

	};
}

#endif