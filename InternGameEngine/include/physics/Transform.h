#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <SFML/System.hpp>

namespace physics
{
	class Transform
	{
		public:
			Transform(const sf::Vector2f& position);
			Transform(const sf::Vector2f& position, float rotation, const sf::Vector2f& scale);
			sf::Vector2f& getPosition();
			void setPosition(const sf::Vector2f& position);
			float getRotation();
			void setRotation(float rotation);
			sf::Vector2f& getScale();
			void setScale(const sf::Vector2f& scale);


		private:
			sf::Vector2f m_position;
			float m_rotation;
			sf::Vector2f m_scale;
	};
}

#endif