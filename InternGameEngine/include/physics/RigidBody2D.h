#ifndef RIGID_BODY_2D_H
#define RIGID_BODY_2D_H

#include <ecs/IComponent.h>

#include <SFML/System.hpp>

namespace physics
{
	class RigidBody2D : public ecs::IComponent
	{
		public:
			RigidBody2D();
			sf::Vector2f& getVelocity();
			void setVelocity(const sf::Vector2f& velocity);
			void setVelocityX(float xVelocity);
			void setVelocityY(float yVelocity);

		private:
			sf::Vector2f m_velocity;
	};
}

#endif