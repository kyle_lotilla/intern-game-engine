#ifndef COLLISION_H
#define COLLISION_H

#include <string>

namespace physics
{
	struct Collision
	{
		std::string tagName;
		int collidedEntityID;
	};
}

#endif