#ifndef COLLIDER_MOVEMENT_SYSTEM_H
#define COLLIDER_MOVEMENT_SYSTEM_H

#include <ecs/ISystem.h>
#include <ecs/ComponentManager.h>
#include <ecs/EntityManager.h>

namespace physics
{
	class ColliderMovementSystem : public ecs::ISystem
	{
		public:
			ColliderMovementSystem(std::shared_ptr<ecs::EntityManager> entityManager, std::shared_ptr<ecs::ComponentManager> componentManager);
			void update(sf::Time deltaTime) const;

		private:
			std::shared_ptr<ecs::EntityManager> m_entityManager;
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
	};
}


#endif