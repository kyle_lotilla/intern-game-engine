#ifndef COLLISION_SYSTEM_H
#define COLLISION_SYSTEM_H

#include <ecs/ComponentManager.h>
#include <ecs/ISystem.h>

namespace physics
{
	class CollisionSystem : public ecs::ISystem
	{
		public:
			CollisionSystem(std::shared_ptr<ecs::ComponentManager> componentManager);
			void update(sf::Time deltaTime) const;

		private:
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
	};
}

#endif