#ifndef BOUNDING_BOX_H
#define BOUNDING_BOX_H

#include <SFML/Graphics.hpp>

namespace physics
{
	class BoundingBox : public sf::FloatRect
	{
		public:
			BoundingBox(const sf::Vector2f& localPosition, const sf::Vector2f& size);
			BoundingBox(float localLeft, float localTop, float width, float height);

			float localLeft;
			float localTop;
	};
}

#endif