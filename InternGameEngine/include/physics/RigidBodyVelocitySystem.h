#ifndef RIGID_BODY_VELOCITY_SYSTEM_H
#define RIGID_BODY_VELOCITY_SYSTEM_H

#include <physics/RigidBody2D.h>
#include <ecs/ISystem.h>
#include <ecs/ComponentManager.h>
#include <ecs/EntityManager.h>

namespace physics
{
	class RigidBodyVelocitySystem : public ecs::ISystem
	{
		public:
			RigidBodyVelocitySystem(const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<ecs::EntityManager>& entityManager);
			void update(sf::Time deltaTime) const;

		private:
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
			std::shared_ptr<ecs::EntityManager> m_entityManager;
	};
}

#endif