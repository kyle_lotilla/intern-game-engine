#ifndef IEVENT_LISTENER_H
#define IEVENT_LISTENER_H

#include <events/Event.h>

#include <string>

namespace events
{
	class IEventListener
	{
		public:
			virtual void eventTriggered(Event event) = 0;
	};
}

#endif