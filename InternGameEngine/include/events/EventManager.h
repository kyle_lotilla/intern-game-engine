#ifndef EVENT_MANAGER_H
#define EVENT_MANAGER_H

#include <events/IEventListener.h>

#include <unordered_map>

namespace events
{
	class EventManager
	{
		public:
			void addEvent(std::string eventName);
			template <typename EventListenerType> void registerToEvent(std::string eventName, const std::shared_ptr<EventListenerType>& eventListener);
			void fireEvent(std::string eventName);
			void fireEvent(Event eventName);
			void clear();

		private:
			std::unordered_map<std::string, std::vector<std::shared_ptr<IEventListener>>> m_eventListenerMap;

	};

	template<typename EventListenerType>
	inline void EventManager::registerToEvent(std::string eventName, const std::shared_ptr<EventListenerType>& eventListener)
	{
		static_assert(std::is_base_of<IEventListener, EventListenerType>::value, "Event Listeners must inherit IEventListener");
		if (m_eventListenerMap.find(eventName) != m_eventListenerMap.end())
		{
			std::vector<std::shared_ptr<IEventListener>>& eventListeners = m_eventListenerMap[eventName];
			eventListeners.push_back(eventListener);
		}
	}
}

#endif