#ifndef EVENT_H
#define EVENT_H

#include <util/ArgsObject.h>

#include <unordered_map>

namespace events
{
	class Event : util::ArgsObject
	{
		public:
			Event(std::string eventName);
			std::string getEventName() const;
			
		private:
			std::string m_eventName;
			
	};
}

#endif