#ifndef BOSS_PROJECTILE_SYSTEM_H
#define BOSS_PROJECTILE_SYSTEM_H

#include <ecs/EntityManager.h>
#include <ecs/ISystem.h>
#include <gameplay/ProjectileShooter.h>

namespace gameplay
{
	class BossProjectileSystem : public ecs::ISystem
	{
		public:
			BossProjectileSystem(int bossID, const std::vector<int>& entityIDPool, const std::shared_ptr<ecs::EntityManager>& entitiyManager, const std::shared_ptr<ecs::ComponentManager>& componentManager);
			void update(sf::Time deltaTime) const;

		private:
			void shootProjectile(const sf::Time& deltaTime, ProjectileShooter& projectileShooter) const;
			void checkOutOfBounds(const sf::Time& deltaTime, ProjectileShooter& projectileShooter) const;

			int m_bossID;
			mutable std::vector<int> m_entityIDPool;
			std::shared_ptr<ecs::EntityManager> m_entityManager;
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
			mutable std::vector<int> m_activeEntityIDPool;
			mutable sf::Time m_timeSinceLastShot;
	};
}

#endif