#ifndef BOSS_PHASE_SPEED_TRANSITION_SYSTEM_H
#define BOSS_PHASE_SPEED_TRANSITION_SYSTEM_H

#include <ecs/ComponentManager.h>
#include <events/IEventListener.h>

namespace gameplay
{
	class BossPhaseSpeedTransitionSystem : public events::IEventListener
	{
		public:
			BossPhaseSpeedTransitionSystem(int bossID, const std::shared_ptr<ecs::ComponentManager>& componentManager);
			void eventTriggered(events::Event event);
		
		private:
			int m_bossID;
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
	};
}

#endif