#ifndef DAMAGE_H
#define DAMAGE_H

#include <ecs/IComponent.h>

namespace gameplay
{
	class Damage : public ecs::IComponent
	{
		public:
			Damage(int damage);
			int getDamage();

		private:
			int m_damage;
	};
}

#endif