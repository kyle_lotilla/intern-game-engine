#ifndef BOSS_MOVEMENT_SYSTEM_H
#define BOSS_MOVEMENT_SYSTEM_H

#include <ecs/ComponentManager.h>
#include <ecs/EntityManager.h>
#include <ecs/ISystem.h>


namespace gameplay
{
	class BossMovementSystem : public ecs::ISystem
	{
		public:
			BossMovementSystem(int bossID, const std::shared_ptr<ecs::EntityManager>& entityManager,
				const std::shared_ptr<ecs::ComponentManager>& componentManager);
			void start() const;
			void update(sf::Time deltaTime) const;

		private:
			int m_bossID;
			std::shared_ptr<ecs::EntityManager> m_entityManager;
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
	};
}


#endif