#ifndef PROJECTILE_SHOOTER_H
#define PROJECTILE_SHOOTER_H

#include <ecs/IComponent.h>

namespace gameplay
{
	class ProjectileShooter : public ecs::IComponent
	{
		public:
			ProjectileShooter(std::string projectilePrefabName, const sf::Time& fireRate, const sf::Vector2f& boundariesX, const sf::Vector2f& boundariesY, const sf::Vector2f& projectileOffset);
			std::string getProjectilePrefabName();
			sf::Time getFireRate();
			void setFireRate(const sf::Time& fireRate);
			sf::Vector2f getBoundariesX();
			void setBoundariesX(const sf::Vector2f& boundariesX);
			sf::Vector2f getBoundariesY();
			void setBoundariesY(const sf::Vector2f& boundariesY);
			sf::Vector2f getProjectileOffest();
			void setProjectileOffset(const sf::Vector2f& projectileOffset);

		private:
			std::string m_projectilePrefabName;
			sf::Time m_fireRate;
			sf::Vector2f m_boundariesX;
			sf::Vector2f m_boundariesY;
			sf::Vector2f m_projectileOffset;

	};
}

#endif