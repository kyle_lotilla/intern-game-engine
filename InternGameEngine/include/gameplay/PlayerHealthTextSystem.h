#ifndef PLAYER_HEALTH_TEXT_SYSTEM_H
#define PLAYER_HEALTH_TEXT_SYSTEM_H

#include <ecs/ComponentManager.h>
#include <ecs/ISystem.h>
#include <events/IEventListener.h>
#include <gameplay/Health.h>
#include <graphics/Text.h>

namespace gameplay
{
	class PlayerHealthTextSystem : public ecs::ISystem, public events::IEventListener
	{
		public:
			PlayerHealthTextSystem(int playerID, int playerHealthTextID, const std::shared_ptr<ecs::ComponentManager>& componentManager);
			void start() const;
			void update(sf::Time deltaTime) const {};
			void eventTriggered(events::Event event);

		private:
			void setHealthText() const;

			int m_playerID;
			int m_playerHealthTextID;
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
	};
}

#endif