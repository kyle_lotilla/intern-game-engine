#ifndef PLAYER_PROJECTIlE_SYSTEM_H
#define PLAYER_PROJECTIlE_SYSTEM_H

#include <ecs/EntityManager.h>
#include <ecs/ISystem.h>
#include <input/KeyboardInputHandler.h>
#include <gameplay/ProjectileShooter.h>

namespace gameplay
{
	class PlayerProjectileSystem : public ecs::ISystem
	{
		public:
			PlayerProjectileSystem(int playerID, const std::vector<int>& entityIDPool, const std::shared_ptr<ecs::EntityManager>& entitiyManager, const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<input::KeyboardInputHandler>& keyboardHandler);
			void update(sf::Time deltaTime) const;

		private:
			void shootProjectile(const sf::Time& deltaTime, ProjectileShooter& projectileShooter) const;
			void checkCollisions(const sf::Time& deltaTime) const;
			void checkOutOfBounds(const sf::Time& deltaTime, ProjectileShooter& projectileShooter) const;

			int m_playerID;
			mutable std::vector<int> m_entityIDPool;
			std::shared_ptr<ecs::EntityManager> m_entityManager;
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
			std::shared_ptr<input::KeyboardInputHandler> m_keyboardHandler;
			mutable std::vector<int> m_activeEntityIDPool;
			mutable sf::Time m_timeSinceLastShot;

	};
}

#endif