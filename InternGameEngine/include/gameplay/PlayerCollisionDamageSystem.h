#ifndef PLAYER_COLLISION_DAMAGE_SYSTEM_H
#define PLAYER_COLLISION_DAMAGE_SYSTEM_H

#include <ecs/ISystem.h>
#include <ecs/ComponentManager.h>
#include <events/EventManager.h>
#include <SFML/Graphics.hpp>

namespace gameplay
{
	class PlayerCollisionDamageSystem : public ecs::ISystem
	{
		public:
			PlayerCollisionDamageSystem(int playerID, const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<events::EventManager>& eventManager);
			void update(sf::Time deltaTime) const;

		private:
			int m_playerID;
			mutable bool m_isInvunrable;
			mutable bool m_isTransparent;
			mutable sf::Time m_timeSinceLastDamage;
			const sf::Time INVUNRABLE_TIME = sf::seconds(1.5f);
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
			std::shared_ptr<events::EventManager> m_eventManager;
	};
}

#endif