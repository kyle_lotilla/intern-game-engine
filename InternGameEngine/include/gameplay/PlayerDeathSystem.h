#ifndef PLAYER_DEATH_SYSTEM_H
#define PLAYER_DEATH_SYSTEM_H

#include <ecs/ISystem.h>
#include <ecs/ComponentManager.h>
#include <events/EventManager.h>

namespace gameplay
{
	class PlayerDeathSystem : public ecs::ISystem
	{
		public:
			PlayerDeathSystem(int playerID, const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<events::EventManager>& eventManager);
			void update(sf::Time deltaTime) const;

		private:
			int m_playerID;
			mutable bool m_isDead;
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
			std::shared_ptr<events::EventManager> m_eventManager;
	};
}

#endif