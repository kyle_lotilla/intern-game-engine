#ifndef GAME_OVER_MENU_SYSTEM_H
#define GAME_OVER_MENU_SYSTEM_H

#include <ecs/ISystem.h>
#include <ecs/ComponentManager.h>
#include <events/IEventListener.h>

namespace gameplay
{
	class GameOverMenuSystem : public ecs::ISystem, public events::IEventListener
	{
		public:
			GameOverMenuSystem(const std::shared_ptr<ecs::ComponentManager>& componentManager);
			void update(sf::Time deltaTime) const;
			void eventTriggered(events::Event event);

		private:
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
			bool m_isGameOver;

	};
}

#endif