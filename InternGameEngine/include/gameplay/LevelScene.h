#ifndef LEVEL_SCENE_H
#define LEVEL_SCENE_H

#include <ecs/SystemManager.h>
#include <ecs/RenderSystemManager.h>
#include <util/ResourceManager.h>
#include <engine/Scene.h>
#include <physics/RigidBodyVelocitySystem.h>
#include <graphics/SpriteRenderSystem.h>
#include <graphics/AnimationControllerRenderSystem.h>
#include <graphics/AnimationSystem.h>
#include <graphics/Text.h>
#include <graphics/TextRenderSystem.h>
#include <graphics/Sprite.h>
#include <input/InputContext.h>
#include <input/ButtonAction.h>
#include <input/MouseButtonAction.h>
#include <input/MouseButtonInputHandler.h>
#include <physics/CollisionSystem.h>
#include <gameplay/PlayerMovementSystem.h>
#include <gameplay/CharacterMovementController.h>
#include <gameplay/BossMovementSystem.h>
#include <physics/ColliderMovementSystem.h>
#include <audio/AudioSource.h>
#include <physics/Collider.h>
#include <gameplay/ProjectileShooter.h>
#include <gameplay/PlayerProjectileSystem.h>
#include <gameplay/BossProjectileSystem.h>
#include <gameplay/Damage.h>
#include <gameplay/Health.h>
#include <gameplay/BossCollisionDamageSystem.h>
#include <gameplay/BossDeathSystem.h>
#include <gameplay/PlayerCollisionDamageSystem.h>
#include <gameplay/PlayerDeathSystem.h>
#include <gameplay/ShowGameOverSystem.h>
#include <gameplay/BossPhaseTransitionSystem.h>
#include <gameplay/BossPhaseSpeedTransitionSystem.h>
#include <gameplay/PlayerHealthTextSystem.h>
#include <ui/MouseUIButton.h>
#include <ui/MouseUIButtonSystem.h>
#include <gameplay/GameOverMenuSystem.h>
#include <json.hpp>
#include <fstream>
#include <Thor/Input.hpp>
#include <engine/EngineRequestComponent.h>

using Json = nlohmann::json;


namespace gameplay
{
	class LevelScene : public engine::Scene
	{
		public:
			LevelScene(const sf::RenderWindow* window);
			void loadScene(util::ArgsObject loadArgs);
			void pollInput();
			void update(const sf::Time& deltaTime);
			void render(float interpolation, sf::RenderTarget& renderTarget);
			void unloadScene();

		private:
			void initializeAllPtr();
			void registerComponents();
			void loadLevel(std::string levelFileLocation);
			int loadCharacter(const Json& characterJson);
			void loadProjectile(std::string projectileFileLocation);
			void loadUI(std::string uiFileLocation);
			void loadInput(std::string inputFileLocation);
			void loadSystems();


			graphics::Sprite loadSprite(const Json& spriteJson);
			graphics::AnimationController loadAnimationController(const Json& animationControllerJson);
			graphics::Animation loadAnimation(const Json& animationControllerJson);
			void loadTexture(std::string fileLocation);
			void loadMusic(const Json& musicJson);
			physics::Transform loadTransform(const Json& transformJson);
			physics::Collider loadCollider(const Json& colliderJson);
			physics::BoundingBox loadBoundingBox(const Json& boundingBoxJson);
			physics::RigidBody2D loadRigidBody2D(const Json& rigidBody2DJson);
			gameplay::CharacterMovementController loadCharacterMovementController(const Json& characterMovementControllerJson);
			gameplay::ProjectileShooter loadProjectileShooter(const Json& projectileShooterJson);
			audio::AudioSource loadAudioSource(const Json& audioSourceJson);
			sf::Sound loadSound(const Json& soundJson);
			void loadSoundBuffer(std::string fileLocation);
			graphics::Text loadText(const Json& textJson);
			void loadFont(std::string fileLocation);

			const sf::RenderWindow* m_window;
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
			std::shared_ptr<ecs::EntityManager> m_entityManager;
			std::shared_ptr<input::InputManager> m_inputManager;
			std::shared_ptr<input::KeyboardInputHandler> m_keyboardInputHandler;
			std::shared_ptr<input::MouseButtonInputHandler> m_mouseButtonInputHandler;
			std::shared_ptr<util::ResourceManager<sf::Texture>> m_textureResourceManager;
			std::shared_ptr<util::ResourceManager<sf::Font>> m_fontResourceManager;
			std::shared_ptr<util::ResourceManager<sf::SoundBuffer>> m_soundBufferResourceManager;
			std::shared_ptr<events::EventManager> m_eventManager;
			std::shared_ptr<audio::MusicManager> m_musicManager;
			ecs::SystemManager m_systemManager;
			ecs::RenderSystemManager m_renderSystemManager;
			int m_playerID;
			int m_bossID;
			int m_healthUIID;
			std::string m_currentLevelLocation;
			std::string m_nextLevelLocation;
	};
}

#endif