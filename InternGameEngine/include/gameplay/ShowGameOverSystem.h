#ifndef SHOW_GAME_OVER_SYSTEM_H
#define SHOW_GAME_OVER_SYSTEM_H

#include <ecs/EntityManager.h>
#include <ecs/ComponentManager.h>
#include <input/InputManager.h>
#include <events/IEventListener.h>
#include <audio/MusicManager.h>

namespace gameplay
{
	class ShowGameOverSystem : public events::IEventListener
	{
		public:
			ShowGameOverSystem(std::string nextLevel, std::shared_ptr<ecs::EntityManager>& entityManager, const std::shared_ptr<ecs::ComponentManager>& componentManager, 
				const std::shared_ptr<input::InputManager>& inputManager, const std::shared_ptr<audio::MusicManager>& musicManager);
			void eventTriggered(events::Event event);

		private:
			bool m_isThereNextLevel;
			std::shared_ptr<ecs::EntityManager> m_entityManager;
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
			std::shared_ptr<input::InputManager> m_inputManager;
			std::shared_ptr<audio::MusicManager> m_musicManager;
	};
}

#endif