#ifndef CHARACTER_MOVEMENT_CONTROLLER_H
#define CHARACTER_MOVEMENT_CONTROLLER_H

#include <SFML/System.hpp>
#include <ecs/IComponent.h>

namespace gameplay
{
	class CharacterMovementController : public ecs::IComponent
	{
	public:
		CharacterMovementController(const sf::Vector2f& characterVelocity, const sf::Vector2f& boundariesX, const sf::Vector2f& boundariesY);
		sf::Vector2f getCharacterVelocity();
		void setCharacterVelocity(const sf::Vector2f& characterVelocity);
		sf::Vector2f getBoundariesX();
		sf::Vector2f getBoundariesY();

	private:
		sf::Vector2f m_characterVelocity;
		sf::Vector2f m_boundariesX;
		sf::Vector2f m_boundariesY;

	};
}

#endif