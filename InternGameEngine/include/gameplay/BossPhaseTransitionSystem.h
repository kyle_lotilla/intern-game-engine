#ifndef BOSS_PHASE_TRANSITION_SYSTEM_H
#define BOSS_PHASE_TRANSITION_SYSTEM_H

#include <ecs/ISystem.h>
#include <ecs/ComponentManager.h>
#include <events/EventManager.h>

#include <SFML/System.hpp>

namespace gameplay
{
	class BossPhaseTransitionSystem : public ecs::ISystem
	{
		public:
			BossPhaseTransitionSystem(int bossID, const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<events::EventManager>& eventManager);
			void update(sf::Time deltaTime) const;

		private:
			int m_bossID;
			mutable bool m_isPhaseTwo;
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
			std::shared_ptr<events::EventManager> m_eventManager;

	};
}


#endif