#ifndef BOSS_COLLISION_DAMAGE_SYSTEM_H
#define BOSS_COLLISION_DAMAGE_SYSTEM_H

#include <ecs/ISystem.h>
#include <ecs/ComponentManager.h>
#include <events/IEventListener.h>
#include <SFML/Graphics.hpp>

namespace gameplay
{
	class BossCollisionDamageSystem : public ecs::ISystem, public events::IEventListener
	{
		public:
			BossCollisionDamageSystem(int bossID, const std::shared_ptr<ecs::ComponentManager>& componentManager);
			void update(sf::Time deltaTime) const;
			void eventTriggered(events::Event event);

		private:
			int m_bossID;
			mutable sf::Time m_timeSinceLastDamage;
			const sf::Time DAMAGE_COLOR_TIME = sf::seconds(0.1f);
			sf::Color m_neutralColor;
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
	};
}

#endif