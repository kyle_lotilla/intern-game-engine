#ifndef MAIN_MENU_SYSTEM_H
#define MAIN_MENU_SYSTEM_H

#include <ecs/ISystem.h>
#include <ecs/ComponentManager.h>

namespace gameplay 
{
	class MainMenuSystem : public ecs::ISystem
	{
		public:
			MainMenuSystem(const std::shared_ptr<ecs::ComponentManager>& componentManager);
			void update(sf::Time deltaTime) const;

		private:
			std::shared_ptr<ecs::ComponentManager> m_componentManager;

	};
}

#endif