#ifndef PLAYER_MOVEMENT_SYSTEM_H
#define PLAYER_MOVEMENT_SYSTEM_H

#include <ecs/ISystem.h>
#include <ecs/ComponentManager.h>
#include <ecs/EntityManager.h>
#include <input/KeyboardInputHandler.h>

namespace gameplay
{
	class PlayerMovementSystem : public ecs::ISystem
	{
		public:
			PlayerMovementSystem(int playerID, const std::shared_ptr<ecs::EntityManager>& entityManager, 
				const std::shared_ptr<ecs::ComponentManager>& componentManager,
				const std::shared_ptr<input::KeyboardInputHandler>& keyboardHandler);
			void update(sf::Time deltaTime) const;

		private:
			int m_playerID;
			std::shared_ptr<ecs::EntityManager> m_entityManager;
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
			std::shared_ptr<input::KeyboardInputHandler> m_keyboardHandler;

	};
}

#endif