#ifndef HEALTH_H
#define HEALTH_H

#include <ecs/IComponent.h>

namespace gameplay
{
	class Health : public ecs::IComponent
	{
		public:
			Health(int baseHealth);
			int getHealth();
			void damageHealth(int damage);
			float getHealthPercent();
			bool isAlive();

		private:
			int m_baseHealth;
			int m_currrentHealth;
	};
}

#endif // !HEALTH_H
