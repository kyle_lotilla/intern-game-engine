#ifndef BOSS_DEATH_SYSTEM_H
#define BOSS_DEATH_SYSTEM_h

#include <ecs/ISystem.h>
#include <ecs/ComponentManager.h>
#include <events/EventManager.h>

namespace gameplay
{
	class BossDeathSystem : public ecs::ISystem, public events::IEventListener
	{
		public:
			BossDeathSystem(int bossID, const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<events::EventManager>& eventManager);
			void update(sf::Time deltaTime) const;
			void eventTriggered(events::Event event);

		private:
			int m_bossID;
			mutable bool m_isDead;
			bool m_isPlayerDead;
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
			std::shared_ptr<events::EventManager> m_eventManager;
	};
}

#endif