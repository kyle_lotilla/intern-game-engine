#ifndef MAIN_MENU_SCENE_H
#define MAIN_MENU_SCENE_H

#include <ecs/ComponentManager.h>
#include <ecs/EntityManager.h>
#include <input/InputManager.h>
#include <ecs/SystemManager.h>
#include <ecs/RenderSystemManager.h>
#include <util/ResourceManager.h>
#include <engine/Scene.h>
#include <input/MouseButtonInputHandler.h>
#include <audio/MusicManager.h>


namespace gameplay
{
	class MainMenuScene : public engine::Scene
	{
		public:
			MainMenuScene(const sf::RenderWindow* window);
			void loadScene(util::ArgsObject loadArgs);
			void pollInput();
			void update(const sf::Time& deltaTime);
			void render(float interpolation, sf::RenderTarget& renderTarget);
			void unloadScene();

		private:
			void initializeAllPtr();
			void registerComponents();
			void loadSystems();

			const sf::RenderWindow* m_window;
			std::shared_ptr<audio::MusicManager> m_musicManager;
			std::shared_ptr<util::ResourceManager<sf::Texture>> m_textureResourceManager;
			std::shared_ptr<util::ResourceManager<sf::Font>> m_fontResourceManager;
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
			std::shared_ptr<ecs::EntityManager> m_entityManager;
			std::shared_ptr<input::InputManager> m_inputManager;
			std::shared_ptr<input::MouseButtonInputHandler> m_mouseButtonInputHandler;
			ecs::SystemManager m_systemManager;
			ecs::RenderSystemManager m_renderSystemManager;
	};
}

#endif