#ifndef MUSIC_MANAGER_H
#define MUSIC_MANAGER_H

#include <SFML/Audio.hpp>
#include <unordered_map>

namespace audio
{
	class MusicManager
	{
		public:
			MusicManager();
			void addMusic(std::string musicName, std::string fileLocation);
			void playMusic(std::string musicName);
			void pauseMusic();
			void stopMusic();
			sf::Music* getMusic(std::string musicName);
			void clear();

		private:
			std::string m_currentMusic;
			std::unordered_map<std::string, std::unique_ptr<sf::Music>> m_musicMap;

	};
}

#endif