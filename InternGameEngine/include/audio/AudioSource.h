#ifndef AUDIO_SOURCE_H
#define AUDIO_SOURCE_H

#include <ecs/IComponent.h>

#include <SFML/Audio.hpp>
#include <unordered_map>

namespace audio
{
	class AudioSource : public ecs::IComponent
	{
		public:
			void addAudioClip(std::string audioClipName, sf::Sound audioClip);
			void playAudioClip(std::string audioClipName);
			void pauseAudioClip(std::string audioClipName);
			void stopAudioClip(std::string audioClipName);
			sf::Sound* getAudioClip(std::string audioClipName);

		private:
			std::unordered_map<std::string, sf::Sound> m_audioClipMap;
	};
}

#endif