#ifndef MOUSE_UI_BUTTON_H
#define MOUSE_UI_BUTTON_H

#include <ecs/IComponent.h>
#include <physics/BoundingBox.h>

namespace ui
{
	class MouseUIButton : public ecs::IComponent
	{
		public:
			MouseUIButton(std::string buttonName, const physics::BoundingBox& boundingBox);
			std::string getButtonName();
			physics::BoundingBox& getBoundingBox();
			bool isPressed();
			void setPressed(bool pressed);

		private:
			std::string m_buttonName;
			physics::BoundingBox m_boundingBox;
			bool m_isPressed;
	};
}

#endif