#ifndef MOUSE_UI_BUTTON_SYSTEM_H
#define MOUSE_UI_BUTTON_SYSTEM_H

#include <ecs/EntityManager.h>
#include <ecs/ISystem.h>
#include <ecs/ComponentManager.h>
#include <input/MouseButtonInputHandler.h>

namespace ui
{
	class MouseUIButtonSystem : public ecs::ISystem
	{
		public:
			MouseUIButtonSystem(const std::shared_ptr<ecs::EntityManager>& entityManager, const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<input::MouseButtonInputHandler>& mouseButtonInputHandler);
			void update(sf::Time deltaTime) const;

		private:
			std::shared_ptr<ecs::EntityManager> m_entityManager;
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
			std::shared_ptr<input::MouseButtonInputHandler> m_mouseButtonInputHandler;
	};
}

#endif