#ifndef  MOUSE_BUTTON_INPUT_MANAGER_H
#define MOUSE_BUTTON_INPUT_MANAGER_H

#include <input/IInputHandler.h>
#include <input/MouseButtonAction.h>
#include <input/InputContext.h>

#include <SFML/Graphics.hpp>
#include <unordered_map>

namespace input
{
	class MouseButtonInputHandler : public IInputHandler
	{
		public:
			MouseButtonInputHandler(const sf::RenderWindow* window);
			void pollInput();
			void setCurrentContext(std::string contextName);
			void clear();
			void addInputContext(const InputContext<sf::Mouse::Button, MouseButtonAction>& inputContext);
			bool isActionPressed(std::string actionName);
			sf::Vector2f getMousePosition(std::string actionName);

		private:
			const sf::RenderWindow* m_window;
			std::string m_currentContext;
			std::unordered_map<std::string, InputContext<sf::Mouse::Button, MouseButtonAction>> m_contextMap;
	};
}

#endif