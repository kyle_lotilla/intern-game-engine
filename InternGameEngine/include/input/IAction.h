#ifndef INPUT_ACTION_H
#define INPUT_ACTION_H

#include <string>

namespace input
{
	class IAction
	{
		public:
			std::string getActionName() const;

		protected:
			IAction(std::string actionName);

		private:
			std::string m_actionName;
	};
}

#endif