#ifndef BUTTON_ACTION_H
#define BUTTON_ACTION_H

#include <input/IAction.h>

#include <string>

namespace input
{
	class ButtonAction : public IAction
	{
		public:
			ButtonAction(std::string actionName);
			bool isPressed() const;
			void setIsPressed(bool isPressed);
			bool isHeldDown() const;
			void setIsHeldDown(bool isHeldDown);

		private:
			bool m_isPressed;
			bool m_isHeldDown;
	};
}

#endif // !BUTTON_ACTION_H
