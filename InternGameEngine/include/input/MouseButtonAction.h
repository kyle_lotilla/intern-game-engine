#ifndef MOUSE_BUTTON_ACTION_H
#define MOUSE_BUTTON_ACTION_H

#include <input/IAction.h>

#include <string>
#include <SFML/System.hpp>

namespace input
{
	class MouseButtonAction : public IAction
	{
		public:
			MouseButtonAction(std::string actionName);
			bool isPressed() const;
			void setIsPressed(bool isPressed);
			bool isHeldDown() const;
			void setIsHeldDown(bool isHeldDown);
			sf::Vector2f getMousePosition() const;
			void setMousePosition(const sf::Vector2f& mousePosition);

		private:
			bool m_isPressed;
			bool m_isHeldDown;
			sf::Vector2f m_mousePosition;
	};
}

#endif // !BUTTON_ACTION_H