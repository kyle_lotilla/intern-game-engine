#ifndef  KEYBOARD_INPUT_HANDLER_H
#define KEYBOARD_INPUT_HANDLER_H

#include <input/IInputHandler.h>
#include <input/ButtonAction.h>
#include <input/InputContext.h>

#include <SFML/Window.hpp>
#include <unordered_map>

namespace input
{
	class KeyboardInputHandler : public IInputHandler
	{
		public:
			KeyboardInputHandler();
			void pollInput();
			void setCurrentContext(std::string contextName);
			void clear();
			void addInputContext(const InputContext<sf::Keyboard::Key, ButtonAction>& inputContext);
			bool isActionPressed(std::string actionName);
			const ButtonAction* getButtonAction(std::string actionName);

		private:
			std::string m_currentContext;
			std::unordered_map<std::string, InputContext<sf::Keyboard::Key, ButtonAction>> m_contextMap;
	};
}

#endif
