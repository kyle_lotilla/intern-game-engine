#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include <input/IInputHandler.h>

#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <vector>

namespace input
{
	class InputManager
	{
		public:
			void pollInput();
			void setCurrentContext(std::string contextName);
			void clear();
			template <typename InputHandlerType> void addInputHandler(const std::shared_ptr<IInputHandler>& inputHandler);

		private:
			std::vector<std::shared_ptr<IInputHandler>> m_inputHandlers;
			std::string m_currentContext;
			
	};

	template <typename InputHandlerType>
	void input::InputManager::addInputHandler(const std::shared_ptr<IInputHandler>& inputHandler)
	{
		static_assert(std::is_base_of<IInputHandler, InputHandlerType>::value, "Input Handler must inherit IInputHandler");
		m_inputHandlers.push_back(inputHandler);
	}
}

#endif