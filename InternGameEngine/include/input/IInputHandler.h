#ifndef INPUT_HANDLER_H
#define INPUT_HANDLER_H

#include <string>

namespace input
{
	class IInputHandler
	{
		public:
			virtual void pollInput() = 0;
			virtual void setCurrentContext(std::string contextName) = 0;
			virtual void clear() = 0;
	};

	
	
}

#endif