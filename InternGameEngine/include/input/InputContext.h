#ifndef INPUT_CONTEXT_H
#define INPUT_CONTEXT_H

#include <input/IAction.h>

#include <unordered_map>
#include <vector>

namespace input
{
	template <typename InputType,  typename ActionType>
	class InputContext
	{
		public:
			InputContext(std::string contextName);
			typename std::vector<std::pair<InputType, ActionType>>::iterator begin();
			typename std::vector<std::pair<InputType, ActionType>>::const_iterator begin() const;
			typename std::vector<std::pair<InputType, ActionType>>::iterator end();
			typename std::vector<std::pair<InputType, ActionType>>::const_iterator end() const;
			std::string getContextName() const;
			bool isActionInContext(std::string actionName) const;
			const std::pair<InputType, ActionType>* getInputActionMapping(std::string actionName) const;
			void addInputActionMapping(const InputType& mappedInput, const ActionType& mappedAction);
			void removeInputActionMapping(std::string actionName);

		private:
			std::string m_contextName;
			std::unordered_map<std::string, int> m_indexMap;
			std::vector<std::pair<InputType, ActionType>> m_inputActionMapping;
	};


	template<typename InputType, typename ActionType>
	InputContext<InputType, ActionType>::InputContext(std::string contextName)
		: m_contextName(contextName)
	{
	}

	template<typename InputType, typename ActionType>
	typename std::vector<std::pair<InputType, ActionType>>::iterator InputContext<InputType, ActionType>::begin()
	{
		return m_inputActionMapping.begin();
	}

	template<typename InputType, typename ActionType>
	typename std::vector<std::pair<InputType, ActionType>>::const_iterator InputContext<InputType, ActionType>::begin() const
	{
		return m_inputActionMapping.begin();
	}

	template<typename InputType, typename ActionType>
	typename std::vector<std::pair<InputType, ActionType>>::iterator InputContext<InputType, ActionType>::end()
	{
		return m_inputActionMapping.end();
	}

	template<typename InputType, typename ActionType>
	typename std::vector<std::pair<InputType, ActionType>>::const_iterator InputContext<InputType, ActionType>::end() const
	{
		return m_inputActionMapping.end();
	}

	template<typename InputType, typename ActionType>
	std::string InputContext<InputType, ActionType>::getContextName() const
	{
		return m_contextName;
	}

	template<typename InputType, typename ActionType>
	bool InputContext<InputType, ActionType>::isActionInContext(std::string actionName) const
	{
		static_assert(std::is_base_of<IAction, ActionType>::value, "ActionType must inherit IAction");

		if (m_indexMap.find(actionName) != m_indexMap.end())
		{
			return true;
		}
		return false;
	}

	template<typename InputType, typename ActionType>
	const std::pair<InputType, ActionType>* InputContext<InputType, ActionType>::getInputActionMapping(std::string actionName) const
	{
		if (isActionInContext(actionName))
		{
			int index = m_indexMap.at(actionName);
			return &(m_inputActionMapping[index]);
		}
		return nullptr;
	}

	template<typename InputType, typename ActionType>
	void InputContext<InputType, ActionType>::addInputActionMapping(const InputType& mappedInput, const ActionType& mappedAction)
	{
		static_assert(std::is_base_of<IAction, ActionType>::value, "ActionType must inherit IAction");

		if (!isActionInContext(mappedAction.getActionName()))
		{
			m_inputActionMapping.push_back({ mappedInput, mappedAction});
			m_indexMap.insert({ mappedAction.getActionName(), m_inputActionMapping.size() - 1});
		}
	}

	template<typename InputType, typename ActionType>
	void InputContext<InputType, ActionType>::removeInputActionMapping(std::string actionName)
	{
		if (isActionInContext(actionName))
		{
			std::pair<InputType, ActionType> lastMapping = m_inputActionMapping.back();
			int elementIndex = m_indexMap[actionName];

			m_inputActionMapping[elementIndex] = lastMapping;
			m_indexMap[lastMapping.second.getActionName()] = elementIndex;

			m_inputActionMapping.pop_back();
			m_indexMap.erase(actionName);
		}
	}

}

#endif