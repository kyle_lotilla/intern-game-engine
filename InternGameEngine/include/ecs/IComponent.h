#ifndef ICOMPONENT_H
#define ICOMPONENT_H

#include <ecs/Entity.h>

namespace ecs
{
	class IComponent
	{
		public:
			int getEntityID();
			void setEntityID(int entityID);

		private:
			int m_entityID;
			
		protected:
			IComponent();
	};
}

#endif

