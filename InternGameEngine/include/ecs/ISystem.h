#ifndef SYSTEM_H
#define SYSTEM_H

#include <SFML/System.hpp>

namespace ecs
{
	class ISystem
	{
		public:
			virtual void start() const {};
			virtual void update(sf::Time deltaTime) const = 0;
	};
}

#endif