#ifndef COMPONENT_ARRAY_H
#define COMPONENT_ARRAY_H

#include <ecs/ECSConstants.h>
#include <ecs/IComponentArray.h>

#include <unordered_map>
#include <array>
#include <vector>

namespace ecs
{
	template<typename ComponentType>
	class ComponentArray : public IComponentArray
	{
		public:
			ComponentArray<ComponentType>();
			ComponentType  &operator[] (int index);
			typename std::vector<ComponentType>::iterator begin() const;
			typename std::vector<ComponentType>::iterator end() const;
			bool hasComponent(int entityID) const;
			bool insertComponent(int entityID, const ComponentType& component);
			typename ComponentType* getComponent(int entityID);
			int getSize() const;
			bool removeComponent(int entityID);
			void registerPrefabComponent(std::string prefabName, const ComponentType& component);
			bool insertPrefabComponent(int entityID, std::string prefabName);

		private:
			mutable std::vector<ComponentType> m_internalComponentArray;
			std::array<int, ecs::MAX_ENTITIES> m_sparseArray;
			std::vector<ComponentType> m_internalPrefabArray;
			std::unordered_map<std::string, int> m_prefabIndexMap;
			int m_size;
	};

	template <typename ComponentType>
	ComponentArray<ComponentType>::ComponentArray()
		: m_internalComponentArray(std::vector<ComponentType>())
	{
		static_assert(std::is_base_of<IComponent, ComponentType>::value, "Component Types must inherit IComponent");
		for (int i = 0; i < ecs::MAX_ENTITIES; i++)
		{
			m_sparseArray[i] = -1;
		}
	}

	template <typename ComponentType>
	ComponentType &ComponentArray<ComponentType>::operator[](int index)
	{
		return m_internalComponentArray[index];
	}

	template <typename ComponentType>
	typename std::vector<ComponentType>::iterator ComponentArray<ComponentType>::begin() const
	{
		return m_internalComponentArray.begin();
	}

	template<typename ComponentType>
	typename std::vector<ComponentType>::iterator ComponentArray<ComponentType>::end() const
	{
		return m_internalComponentArray.end();
	}

	template<typename ComponentType>
	inline bool ComponentArray<ComponentType>::hasComponent(int entityID) const
	{
		if (entityID < 0 || entityID >= ecs::MAX_ENTITIES || m_sparseArray[entityID] < 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	template<typename ComponentType>
	bool ComponentArray<ComponentType>::insertComponent(int entityID, const ComponentType& component)
	{
		if (!hasComponent(entityID))
		{
			m_internalComponentArray.push_back(component);
			m_size = m_internalComponentArray.size();
			m_sparseArray[entityID] = m_size - 1;
			m_internalComponentArray.back().setEntityID(entityID);
			return true;
		}
		return false;
	}

	template<typename ComponentType>
	inline typename ComponentType * ComponentArray<ComponentType>::getComponent(int entityID)
	{
		if (!hasComponent(entityID))
		{
			return nullptr;
		}
		else
		{
			return &(m_internalComponentArray[m_sparseArray[entityID]]);
		}
	}

	template <typename ComponentType>
	int ComponentArray<ComponentType>::getSize() const
	{
		return m_size;
	}

	template<typename ComponentType>
	inline bool ComponentArray<ComponentType>::removeComponent(int entityID)
	{
		if (hasComponent(entityID))
		{
			ComponentType lastComponent = m_internalComponentArray.back();
			int entityIndex = m_sparseArray[entityID];

			m_internalComponentArray[entityIndex] = lastComponent;
			m_sparseArray[lastComponent.getEntityID()] = entityIndex;

			m_sparseArray[entityID] = -1;
			m_internalComponentArray.pop_back();
			m_size--;
			return true;
		}
		return false;
	}

	template<typename ComponentType>
	inline void ComponentArray<ComponentType>::registerPrefabComponent(std::string prefabName, const ComponentType& component)
	{
		if (m_prefabIndexMap.find(prefabName) == m_prefabIndexMap.end())
		{
			m_internalPrefabArray.push_back(component);
			m_prefabIndexMap.insert({ prefabName, m_internalPrefabArray.size() - 1 });
		}
	}

	template<typename ComponentType>
	inline bool ComponentArray<ComponentType>::insertPrefabComponent(int entityID, std::string prefabName)
	{
		if (m_prefabIndexMap.find(prefabName) != m_prefabIndexMap.end())
		{
			return insertComponent(entityID, m_internalPrefabArray[m_prefabIndexMap[prefabName]]);
		}
		return false;
	}

}

#endif