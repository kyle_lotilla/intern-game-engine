#ifndef ENTITY_MANAGER_H
#define ENTITY_MANAGER_H

#include <ecs/Entity.h>
#include <ecs/ECSConstants.h>
#include <ecs/ComponentManager.h>
#include <physics/Transform.h>

#include <queue>


namespace ecs
{
	class EntityManager 
	{
		public:
			EntityManager(std::shared_ptr<ComponentManager> componentManager);
			int createEntity();
			int createEntity(std::string prefabName);
			int createEntity(const physics::Transform& transform);
			int createEntity(std::string prefabName, const physics::Transform& transform);
			std::vector<int> createEntities(std::size_t numEntities);
			std::vector<int> createEntities(std::string prefabName, std::size_t numEntities);
			Entity* getEntity(int entityID);
			void destroyEntity(int entityID);

		private:
			std::queue<int> m_availableIDs;
			std::array<int, ecs::MAX_ENTITIES> m_sparseArray;
			std::vector<Entity> m_entities;
			int m_activeEntityCount;
			std::shared_ptr<ComponentManager> m_componentManager;
	};
}

#endif
