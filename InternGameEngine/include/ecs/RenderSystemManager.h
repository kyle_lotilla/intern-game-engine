#ifndef RENDER_SYSTEM_MANAGER_H
#define RENDER_SYSTEM_MANAGER_H

#include <ecs/IRenderSystem.h>

namespace ecs
{
	class RenderSystemManager
	{
		public:
			template <typename RenderSystemType> void addSystem(const std::shared_ptr<IRenderSystem>& renderSystem);
			void render(float interpolation, sf::RenderTarget& renderTarget);
			void clear();

		private:
			std::vector<std::shared_ptr<IRenderSystem>> m_renderSystems;
	};

	template <typename RenderSystemType>
	void ecs::RenderSystemManager::addSystem(const std::shared_ptr<IRenderSystem>& renderSystem)
	{
		static_assert(std::is_base_of<IRenderSystem, RenderSystemType>::value, "Render System must inherit IRenderSystem");
		m_renderSystems.push_back(renderSystem);
	}
}

#endif