#ifndef COMPONENT_MANAGER_H
#define COMPONENT_MANAGER_H

#include <ecs/IComponent.h>
#include <ecs/ComponentArray.h>

#include <unordered_map>

namespace ecs
{
	class ComponentManager
	{
		
		public:
			template <typename ComponentType> void registerComponent();
			template <typename ComponentType> void unregisterComponent();
			template <typename ComponentType> bool insertComponent(int entityID, ComponentType& component);
			template <typename ComponentType> bool removeComponent(int entityID);
			void removeAllComponents(int entityID);
			template <typename ComponentType> bool hasComponent(int entityID);
			template <typename ComponentType> ComponentType* getComponent(int entityID);
			template <typename ComponentType> const ecs::ComponentArray<ComponentType>* getComponentArray();
			template <typename ComponentType> void registerPrefabComponent(std::string prefabName, const ComponentType& component);
			template <typename ComponentType> bool insertPrefabComponent(int entityID, std::string prefabName);
			void instantiatePrefab(int entityID, std::string prefabName);
			void reset();

		private:
			template <typename ComponentType> bool isComponentRegistered();
			template <typename ComponentType> ecs::ComponentArray<ComponentType>* getInternalComponentArray();

			std::unordered_map<std::string, int> m_indexMap;
			std::vector<std::string> m_componentNames;
			std::vector<std::unique_ptr<IComponentArray>> m_componentArrays;
	};

	template<typename ComponentType>
	bool ComponentManager::isComponentRegistered()
	{
		if (m_indexMap.find(typeid(ComponentType).name()) == m_indexMap.end())
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	template<typename ComponentType>
	inline ecs::ComponentArray<ComponentType>* ComponentManager::getInternalComponentArray()
	{
		if (isComponentRegistered<ComponentType>())
		{
			return static_cast<ComponentArray<ComponentType>*>(m_componentArrays[m_indexMap[typeid(ComponentType).name()]].get());
		}
		else
		{
			return nullptr;
		}
	}
	
	template<typename ComponentType>
	void ComponentManager::registerComponent()
	{
		if (!isComponentRegistered<ComponentType>())
		{
			static_assert(std::is_base_of<IComponent, ComponentType>::value, "Component Types must inherit IComponent");
			m_componentArrays.emplace_back(std::make_unique<ComponentArray<ComponentType>>());
			std::string componentName = typeid(ComponentType).name();
			m_componentNames.push_back(componentName);
			m_indexMap.insert({ componentName, m_componentArrays.size() - 1 });
		}
	}

	template<typename ComponentType>
	void ComponentManager::unregisterComponent()
	{
		if (isComponentRegistered<ComponentType>())
		{
			std::string lastComponentName = m_componentNames.back();
			std::string componentName = typeid(ComponentType).name();
			int componentIndex = m_indexMap[componentName];
			
			m_componentNames[componentIndex] = lastComponentName;
			m_componentArrays[componentIndex] = std::move(m_componentArrays.back());
			m_indexMap[lastComponentName] = componentIndex;

			m_componentNames.pop_back();
			m_componentArrays.pop_back();
			m_indexMap.erase(componentName);
		}
	}

	template<typename ComponentType>
	bool ComponentManager::insertComponent(int entityID, ComponentType& component)
	{
		if (isComponentRegistered<ComponentType>())
		{
			static_assert(std::is_base_of<IComponent, ComponentType>::value, "Component Types must inherit IComponent");
			ecs::ComponentArray<ComponentType>* componentArray = getInternalComponentArray<ComponentType>();
			if (componentArray)
			{
				return componentArray->insertComponent(entityID, component);
			}
		}
		return false;
	}

	template<typename ComponentType>
	inline bool ComponentManager::removeComponent(int entityID)
	{
		if (isComponentRegistered<ComponentType>())
		{
			ecs::ComponentArray<ComponentType>* componentArray = getInternalComponentArray<ComponentType>();
			if (componentArray)
			{
				return componentArray->removeComponent(entityID);
			}
		}
		return false;
	}

	inline void ComponentManager::removeAllComponents(int entityID)
	{
		for (std::unique_ptr<IComponentArray>& componentArray : m_componentArrays)
		{
			componentArray->removeComponent(entityID);
		}
	}

	template<typename ComponentType>
	inline bool ComponentManager::hasComponent(int entityID)
	{
		if (isComponentRegistered<ComponentType>())
		{
			ecs::ComponentArray<ComponentType>* componentArray = getInternalComponentArray<ComponentType>();
			if (componentArray)
			{
				return componentArray->hasComponent(entityID);
			}
			return false;
		}
		else
		{
			return false;
		}
	}

	template<typename ComponentType>
	inline ComponentType* ComponentManager::getComponent(int entityID)
	{
		if (isComponentRegistered<ComponentType>())
		{
			ecs::ComponentArray<ComponentType>* componentArray = getInternalComponentArray<ComponentType>();
			if (componentArray)
			{
				return componentArray->getComponent(entityID);
			}
			return nullptr;
		}
		else
		{
			return nullptr;
		}
	}
	
	template<typename ComponentType>
	const ComponentArray<ComponentType>* ComponentManager::getComponentArray()
	{
		if (isComponentRegistered<ComponentType>())
		{
			return static_cast<ComponentArray<ComponentType>*>(m_componentArrays[m_indexMap[typeid(ComponentType).name()]].get());
		}
		else
		{
			return nullptr;
		}
	}

	template<typename ComponentType>
	inline void ComponentManager::registerPrefabComponent(std::string prefabName, const ComponentType& component)
	{
		if (isComponentRegistered<ComponentType>())
		{
			ComponentArray<ComponentType>* componentArray = getInternalComponentArray<ComponentType>();
			if (componentArray)
			{
				componentArray->registerPrefabComponent(prefabName, component);
			}
		}
	}

	template<typename ComponentType>
	inline bool ComponentManager::insertPrefabComponent(int entityID, std::string prefabName)
	{
		if (isComponentRegistered<ComponentType>())
		{
			ComponentArray<ComponentType>* componentArray = getInternalComponentArray<ComponentType>();
			if (componentArray)
			{
				componentArray->insertPrefabComponent(entityID, prefabName);
			}
		}
	}

	inline void ComponentManager::instantiatePrefab(int entityID, std::string prefabName)
	{
		for (std::unique_ptr<IComponentArray>& componentArray : m_componentArrays)
		{
			componentArray->insertPrefabComponent(entityID, prefabName);
		}
	}

	inline void ComponentManager::reset()
	{
		m_indexMap.clear();
		m_componentNames.clear();
		m_componentArrays.clear();
	}
}

#endif
