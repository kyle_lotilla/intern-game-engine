#ifndef SYSTEM_MANAGER_H
#define SYSTEM_MANAGER_H

#include <ecs/ISystem.h>

#include <SFML/System.hpp>
#include <vector>

namespace ecs
{
	class SystemManager
	{
		public:
			template <typename SystemType> void addSystem(const std::shared_ptr<ISystem>& system);
			void start();
			void update(const sf::Time& deltaTime);
			void clear();

		private:
			std::vector<std::shared_ptr<ISystem>> m_systems;

	};

	template <typename SystemType>
	void ecs::SystemManager::addSystem(const std::shared_ptr<ISystem>& system)
	{
		static_assert(std::is_base_of<ISystem, SystemType>::value, "System must inherit ISystem");
		m_systems.push_back(system);
	}
}

#endif // !SYSTEM_MANAGER_H
