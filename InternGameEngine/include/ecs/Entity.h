#ifndef ENTITY_H
#define ENTITY_H

#include <physics/Transform.h>

namespace ecs
{
	class Entity
	{
		public:
			Entity(int id);
			Entity(int id, const physics::Transform& transform);
			int getID() const;
			physics::Transform& getTransform();
			void setTransfrom(const physics::Transform& transform);

		private:
			int m_ID;
			physics::Transform m_transform;
	};
}

#endif