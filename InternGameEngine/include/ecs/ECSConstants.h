#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <iostream>

namespace ecs
{
	const int MAX_ENTITIES = 1000;
	const int MAX_SYSTEMS = 100;
}

#endif