#ifndef IRENDER_SYSTEM_H
#define IRENDER_SYSTEM_H

#include <SFML/Graphics.hpp>

namespace ecs
{
	class IRenderSystem
	{
		public:
			virtual void render(float interpolation, sf::RenderTarget& renderTarget, int layer) = 0;
	};
}

#endif