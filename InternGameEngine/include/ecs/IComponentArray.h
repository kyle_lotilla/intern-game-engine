#ifndef ICOMPONENT_ARRAY_H
#define ICOMPONENT_ARRAY_H

namespace ecs
{
	class IComponentArray
	{
		public:
			virtual bool insertPrefabComponent(int entityID, std::string prefabName) = 0;
			virtual bool removeComponent(int entityID) = 0;
	};
}

#endif // !ICOMPONENT_ARRAY_H
