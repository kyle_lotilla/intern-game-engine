#ifndef ENGINE_REQUEST_COMPONENT_H
#define ENGINE_REQUEST_COMPONENT_H

#include <ecs/IComponent.h>
#include <util/ArgsObject.h>

namespace engine
{
	struct EngineRequestComponent : public ecs::IComponent
	{
		std::string engineRequest;
	};
}

#endif