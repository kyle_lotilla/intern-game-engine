#ifndef SCENE_H
#define SCENE_H

#include <util/ArgsObject.h>

#include <SFML/Graphics.hpp>

namespace engine
{
	class Scene
	{
		public:
			Scene();
			virtual void loadScene(util::ArgsObject loadArgs) {};
			virtual void pollInput() = 0;
			virtual void update(const sf::Time& deltaTime) = 0;
			virtual void render(float interpolation, sf::RenderTarget& renderTarget) = 0;
			virtual void unloadScene() = 0;
			void requestLoadScene(std::string sceneName, util::ArgsObject loadArgs = util::ArgsObject());
			void requestQuitGame(util::ArgsObject quitArgs = util::ArgsObject());
			std::string getEngineRequest();
			const util::ArgsObject& getEngineRequestArgs();
			void clearRequest();

		private:
			std::string m_engineRequest;
			util::ArgsObject m_engineRequestArgs;
	};
}

#endif