#ifndef GAMEAPP_H
#define GAMEAPP_H

#include <engine/Scene.h>

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <unordered_map>

namespace engine 
{
	class GameApp
	{
		public:
			GameApp();
			GameApp(int width, int height, const std::string& title);
			void run();
			void stop();
			sf::RenderWindow& getWindow();
			template <typename SceneType> void addScene(std::string sceneName, const SceneType& scene);
			void loadScene(std::string sceneName, util::ArgsObject loadArgs = util::ArgsObject());

		private:
			virtual void initialize() {};
			void processEvents();
			void processEngineRequest();
			void update(const sf::Time& deltaTime);
			void render(float interpolation);
			void gameLoop();

		protected:
			const sf::Time TIME_PER_UPDATE = sf::seconds(1.f / 60.f);
			sf::RenderWindow m_renderWindow;
			bool m_isRunning;
			std::unordered_map<std::string, std::unique_ptr<Scene>> m_sceneMap;
			Scene* m_currentScene;
			sf::Clock m_clock;

	};
	template<typename SceneType>
	inline void GameApp::addScene(std::string sceneName, const SceneType& scene)
	{
		if (m_sceneMap.find(sceneName) == m_sceneMap.end())
		{
			m_sceneMap.emplace(sceneName, std::make_unique<SceneType>(scene));
		}
	}
}
#endif