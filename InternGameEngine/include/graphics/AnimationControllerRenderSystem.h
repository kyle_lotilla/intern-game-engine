#ifndef ANIMATION_CONTROLLER_RENDER_SYSTEM_H
#define ANIMATION_CONTROLLER_RENDER_SYSTEM_H

#include <ecs/ComponentManager.h>
#include <ecs/EntityManager.h>
#include <ecs/IRenderSystem.h>


namespace graphics
{
	class AnimationControllerRenderSystem : public ecs::IRenderSystem
	{
		public:
			AnimationControllerRenderSystem(const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<ecs::EntityManager>& entityManager);
			void render(float interpolation, sf::RenderTarget& renderTarget, int layer);

		private:
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
			std::shared_ptr<ecs::EntityManager> m_entityManager;
			std::vector<std::vector<int>> m_layers;
	};
}

#endif