#ifndef SPRITE_H
#define SPRITE_H

#include <ecs/IComponent.h>
#include <physics/Transform.h>

#include <SFML/Graphics.hpp>

namespace graphics
{
	class Sprite : public sf::Sprite, public ecs::IComponent
	{
		public:
			Sprite();
			Sprite(const sf::Vector2f& initialPosition, const sf::Texture& texture);
			sf::Vector2f getCurrentPosition();
			void setCurrentPosition(const sf::Vector2f& currentPosition);
			sf::Vector2f getPreviousPosition();
			void setPreviousPosition(const sf::Vector2f& previousPosition);
			int getLayer();
			void setLayer(int layer);

		private:
			sf::Vector2f m_currentPosition;
			sf::Vector2f m_previousPosition;
			int m_layer;
	};
}

#endif