#ifndef ANIMATION_H
#define ANIMATION_H

#include <vector>
#include <SFML/Graphics.hpp>

namespace graphics
{
	class Animation
	{
		public:
			Animation();
			sf::Time getTimePerFrame();
			void setTimePerFrame(const sf::Time& timePerFrame);
			bool isLooped();
			void setLooped(bool isLoop);
			void setSpriteSheet(const sf::Texture& spriteSheet);
			void addFrame(const sf::IntRect& rect);
			const sf::Texture* getSpriteSheet() const;
			std::size_t getFrameCount();
			sf::IntRect& getFrame(std::size_t frameNumber);

		private:
			sf::Time m_timePerFrame;
			bool m_isLooped;
			std::vector<sf::IntRect> m_frames;
			const sf::Texture* m_spriteSheet;
	};
}

#endif