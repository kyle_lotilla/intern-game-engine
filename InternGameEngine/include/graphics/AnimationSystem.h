#ifndef ANIMATION_SYSTEM_H
#define ANIMATION_SYSTEM_H

#include <ecs/IRenderSystem.h>
#include <ecs/ISystem.h>
#include <ecs/ComponentManager.h>
#include <ecs/EntityManager.h>
#include <graphics/AnimationController.h>

namespace graphics
{
	class AnimationSystem : public ecs::ISystem
	{
		public:
			AnimationSystem(const std::shared_ptr<ecs::ComponentManager>& componentManager);
			void update(sf::Time deltaTime) const;

		private:
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
	};
}

#endif