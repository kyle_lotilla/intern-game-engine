#ifndef ANIMATION_CONTROLLER_H
#define ANIMATION_CONTROLLER_H

#include <ecs/IComponent.h>
#include <graphics/Animation.h>

#include <unordered_map>

namespace graphics
{
	class AnimationController : public ecs::IComponent, public sf::Drawable, public sf::Transformable
	{
		public:
			AnimationController();
			AnimationController(const sf::Vector2f& initialPosition, const sf::Time& timePerFrame, bool isPlaying, bool isLooped);
			void addAnimation(std::string animationName, const Animation& animation);
			Animation* getCurrentAnimation();
			void setCurrentAnimation(std::string animationName);
			void setTimePerFrame(const sf::Time& timePerFrame);
			sf::Time getTimePerFrame();
			void setCurrentTime(const sf::Time& currentTime);
			sf::Time getCurrentTime();
			std::size_t getCurrentFrameNumber();
			void setCurrentFrame(std::size_t frame);
			void setColor(const sf::Color& color);
			void play();
			void pause();
			void stop();
			bool isPlaying();
			bool isLooped();
			void setLooped(bool isLooped);
			sf::Vector2f getCurrentPosition();
			void setCurrentPosition(const sf::Vector2f& currentPosition);
			sf::Vector2f getPreviousPosition();
			void setPreviousPosition(const sf::Vector2f& previousPosition);
			int getLayer();
			void setLayer(int layer);

		private:
			bool hasAnimation() const;
			virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

			std::unordered_map<std::string, Animation> m_animations;
			sf::Vector2f m_currentPosition;
			sf::Vector2f m_previousPosition;
			std::string m_currentAnimation;
			sf::Time m_timePerFrame;
			sf::Time m_currentTime;
			std::size_t m_currentFrame;
			bool m_isPlaying;
			bool m_isLooped;
			sf::Vertex m_vertices[4];
			int m_layer;
	};
}

#endif