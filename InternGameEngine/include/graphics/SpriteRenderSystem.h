#ifndef SPRITE_RENDER_SYSTEM_H
#define SPRITE_RENDER_SYSTEM_H

#include <ecs/EntityManager.h>
#include <ecs/ComponentManager.h>
#include <ecs/IRenderSystem.h>

namespace graphics
{
	class SpriteRenderSystem : public ecs::IRenderSystem
	{
		public:
			SpriteRenderSystem(const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<ecs::EntityManager>& entityManager);
			void render(float interpolation, sf::RenderTarget& renderTarget, int layer);

		private:
			std::shared_ptr<ecs::ComponentManager> m_componentManager;
			std::shared_ptr<ecs::EntityManager> m_entityManager;
	};
}

#endif