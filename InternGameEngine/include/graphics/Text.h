#ifndef TEXT_H
#define TEXT_H

#include <ecs/IComponent.h>
#include <SFML/Graphics.hpp>

namespace graphics
{
	class Text : public ecs::IComponent, public sf::Text
	{
		public:
			Text();
			sf::Vector2f getCurrentPosition();
			void setCurrentPosition(const sf::Vector2f& currentPosition);
			sf::Vector2f getPreviousPosition();
			void setPreviousPosition(const sf::Vector2f& previousPosition);
			sf::Vector2f getLocalPosition();
			void setLocalPosition(const sf::Vector2f& localPosition);
			int getLayer();
			void setLayer(int layer);

		private:
			sf::Vector2f m_currentPosition;
			sf::Vector2f m_previousPosition;
			sf::Vector2f m_localPosition;
			int m_layer;
	};
}

#endif