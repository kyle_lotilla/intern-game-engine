#include <graphics/AnimationController.h>

graphics::AnimationController::AnimationController()
	: m_currentAnimation("NONE"), m_timePerFrame(sf::seconds(0.2f)), m_currentTime(sf::Time::Zero), m_currentFrame(0), m_isPlaying(true), m_isLooped(false),
	m_currentPosition(0.0f,0.0f), m_previousPosition(0.0f,0.0f), m_layer(1)
{
}



graphics::AnimationController::AnimationController(const sf::Vector2f& initialPosition, const sf::Time& timePerFrame, bool isPlaying, bool isLooped)
	: m_currentAnimation("NONE"), m_timePerFrame(timePerFrame), m_currentTime(sf::Time::Zero), m_currentFrame(0), m_isLooped(isLooped),
	m_currentPosition(initialPosition), m_previousPosition(initialPosition)
{
}

void graphics::AnimationController::addAnimation(std::string animationName, const Animation& animation)
{
	if (m_animations.find(animationName) == m_animations.end())
	{
		m_animations.insert({ animationName, animation });

		if (!hasAnimation())
		{
			setCurrentAnimation(animationName);
		}
	}
}

graphics::Animation* graphics::AnimationController::getCurrentAnimation()
{
	if (hasAnimation())
		return &(m_animations.at(m_currentAnimation));
	else
		return nullptr;
}

void graphics::AnimationController::setCurrentAnimation(std::string animationName)
{
	if (m_animations.find(animationName) != m_animations.end())
	{
		m_currentAnimation = animationName;
		m_currentTime = sf::Time::Zero;
		Animation& currentAnimation = m_animations[m_currentAnimation];
		m_timePerFrame = currentAnimation.getTimePerFrame();
		m_isLooped = currentAnimation.isLooped();
		setCurrentFrame(0);
		play();
	}
}

void graphics::AnimationController::setTimePerFrame(const sf::Time& timePerFrame)
{
	m_timePerFrame = timePerFrame;
}

sf::Time graphics::AnimationController::getTimePerFrame()
{
	return m_timePerFrame;
}

void graphics::AnimationController::setCurrentTime(const sf::Time& currentTime)
{
	if (currentTime < m_timePerFrame)
	{
		m_currentTime = currentTime;
	}
}

sf::Time graphics::AnimationController::getCurrentTime()
{
	return m_currentTime;
}

std::size_t graphics::AnimationController::getCurrentFrameNumber()
{
	return m_currentFrame;
}

void graphics::AnimationController::setCurrentFrame(std::size_t frame)
{
	if (hasAnimation())
	{
		Animation& currentAnimation = m_animations[m_currentAnimation];
		if (frame < currentAnimation.getFrameCount())
		{
			m_currentFrame = frame;
			sf::IntRect frameRect = currentAnimation.getFrame(frame);

			m_vertices[0].position = sf::Vector2f(0.f, 0.f);
			m_vertices[1].position = sf::Vector2f(0.f, static_cast<float>(frameRect.height));
			m_vertices[2].position = sf::Vector2f(static_cast<float>(frameRect.width), static_cast<float>(frameRect.height));
			m_vertices[3].position = sf::Vector2f(static_cast<float>(frameRect.width), 0.f);

			float left = static_cast<float>(frameRect.left) + 0.0001f;
			float right = left + static_cast<float>(frameRect.width);
			float top = static_cast<float>(frameRect.top);
			float bottom = top + static_cast<float>(frameRect.height);

			m_vertices[0].texCoords = sf::Vector2f(left, top);
			m_vertices[1].texCoords = sf::Vector2f(left, bottom);
			m_vertices[2].texCoords = sf::Vector2f(right, bottom);
			m_vertices[3].texCoords = sf::Vector2f(right, top);
		}
	}
}

void graphics::AnimationController::setColor(const sf::Color & color)
{
	m_vertices[0].color = color;
	m_vertices[1].color = color;
	m_vertices[2].color = color;
	m_vertices[3].color = color;
}

void graphics::AnimationController::play()
{
	m_isPlaying = true;
}

void graphics::AnimationController::pause()
{
	m_isPlaying = false;
}

void graphics::AnimationController::stop()
{
	m_isPlaying = false;
	m_currentTime = sf::Time::Zero;
}

bool graphics::AnimationController::isPlaying()
{
	return m_isPlaying;
}

bool graphics::AnimationController::isLooped()
{
	return m_isLooped;
}

void graphics::AnimationController::setLooped(bool isLooped)
{
	m_isLooped = isLooped;
}

sf::Vector2f graphics::AnimationController::getCurrentPosition()
{
	return m_currentPosition;
}

void graphics::AnimationController::setCurrentPosition(const sf::Vector2f& currentPosition)
{
	m_currentPosition = currentPosition;
}

sf::Vector2f graphics::AnimationController::getPreviousPosition()
{
	return m_previousPosition;
}

void graphics::AnimationController::setPreviousPosition(const sf::Vector2f & previousPosition)
{
	m_previousPosition = previousPosition;
}

int graphics::AnimationController::getLayer()
{
	return m_layer;
}

void graphics::AnimationController::setLayer(int layer)
{
	m_layer = layer;
}

bool graphics::AnimationController::hasAnimation() const
{
	if (m_animations.size() > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void graphics::AnimationController::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (hasAnimation())
	{
		states.transform *= getTransform();
		states.texture = m_animations.at(m_currentAnimation).getSpriteSheet();
		target.draw(m_vertices, 4, sf::Quads, states);
	}
}
