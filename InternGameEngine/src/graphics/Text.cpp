#include <graphics/Text.h>

graphics::Text::Text()
	: m_currentPosition(sf::Vector2f(0.0f, 0.0f)), m_previousPosition(sf::Vector2f(0.0f, 0.0f)), m_localPosition(sf::Vector2f(0.0f, 0.0f)), m_layer(2)
{
}

sf::Vector2f graphics::Text::getCurrentPosition()
{
	return m_currentPosition;
}

void graphics::Text::setCurrentPosition(const sf::Vector2f & currentPosition)
{
	m_currentPosition = currentPosition;
}

sf::Vector2f graphics::Text::getPreviousPosition()
{
	return m_previousPosition;
}

void graphics::Text::setPreviousPosition(const sf::Vector2f & previousPosition)
{
	m_previousPosition = previousPosition;
}

sf::Vector2f graphics::Text::getLocalPosition()
{
	return m_localPosition;
}

void graphics::Text::setLocalPosition(const sf::Vector2f& localPosition)
{
	m_localPosition = localPosition;
}

int graphics::Text::getLayer()
{
	return m_layer;
}

void graphics::Text::setLayer(int layer)
{
	m_layer = layer;
}


