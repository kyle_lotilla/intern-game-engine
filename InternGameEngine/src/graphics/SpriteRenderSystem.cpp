#include <graphics/SpriteRenderSystem.h>
#include <physics/Transform.h>
#include <graphics/Sprite.h>

graphics::SpriteRenderSystem::SpriteRenderSystem(const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<ecs::EntityManager>& entityManager)
	: m_componentManager(componentManager), m_entityManager(entityManager)
{
}

void graphics::SpriteRenderSystem::render(float interpolation, sf::RenderTarget& renderTarget, int layer)
{
	const ecs::ComponentArray<Sprite>* spriteArray = m_componentManager->getComponentArray<Sprite>();
	if (spriteArray)
	{
		for (Sprite& sprite : *spriteArray)
		{
			if (sprite.getLayer() == layer)
			{
				ecs::Entity* entity = m_entityManager->getEntity(sprite.getEntityID());
				if (entity)
				{
					physics::Transform& transform = entity->getTransform();
					sprite.setPreviousPosition(sprite.getCurrentPosition());
					sprite.setCurrentPosition(transform.getPosition());

					sprite.setPosition(sprite.getCurrentPosition() * interpolation + sprite.getPreviousPosition() * (1.0f - interpolation));
					renderTarget.draw(sprite);
				}
			}
		}
	}
}