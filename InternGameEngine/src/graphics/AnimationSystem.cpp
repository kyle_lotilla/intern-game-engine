#include <graphics/AnimationSystem.h>
#include <physics/Transform.h>
#include <ecs/Entity.h>
#include <ecs/ComponentArray.h>

graphics::AnimationSystem::AnimationSystem(const std::shared_ptr<ecs::ComponentManager>& componentManager)
	: m_componentManager(componentManager)
{
}

void graphics::AnimationSystem::update(sf::Time deltaTime) const
{
	const ecs::ComponentArray<AnimationController>* animationControllers = m_componentManager->getComponentArray<AnimationController>();

	for (AnimationController& animationController : *animationControllers)
	{
		if (animationController.isPlaying())
		{
			Animation* currentAnimation = animationController.getCurrentAnimation();
			std::size_t currentFrame = animationController.getCurrentFrameNumber();
			sf::Time currentTime = animationController.getCurrentTime();
			sf::Time timePerFrame = animationController.getTimePerFrame();

			currentTime += deltaTime;

			if (currentTime >= timePerFrame)
			{
				animationController.setCurrentTime(sf::microseconds(currentTime.asMicroseconds() % timePerFrame.asMicroseconds()));

				if (currentFrame + 1 < currentAnimation->getFrameCount())
				{
					animationController.setCurrentFrame(currentFrame + 1);
				}
				else
				{
					if (animationController.isLooped())
					{
						animationController.setCurrentFrame(0);
					}
					else
					{
						animationController.stop();
					}
				}
			}
			else
			{
				animationController.setCurrentTime(currentTime);
			}
		}
	}
}
