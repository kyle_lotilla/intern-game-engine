#include <graphics/AnimationControllerRenderSystem.h>
#include <graphics/AnimationController.h>
#include <graphics/GraphicsContants.h>

graphics::AnimationControllerRenderSystem::AnimationControllerRenderSystem(const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<ecs::EntityManager>& entityManager)
	: m_componentManager(componentManager), m_entityManager(entityManager)
{
}



void graphics::AnimationControllerRenderSystem::render(float interpolation, sf::RenderTarget& renderTarget, int layer)
{
	const ecs::ComponentArray<AnimationController>* animationControllers = m_componentManager->getComponentArray<AnimationController>();

	for (AnimationController& animationController : *animationControllers)
	{
		if (animationController.getLayer() == layer)
		{
			ecs::Entity* entity = m_entityManager->getEntity(animationController.getEntityID());

			if (entity)
			{
				physics::Transform transform = entity->getTransform();
				animationController.setPreviousPosition(animationController.getCurrentPosition());
				animationController.setCurrentPosition(transform.getPosition());
				animationController.setPosition(animationController.getCurrentPosition() * interpolation + animationController.getPreviousPosition() * (1.0f - interpolation));
				renderTarget.draw(animationController);
			}
		}
	}
	
}
