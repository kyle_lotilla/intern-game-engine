#include <graphics/Sprite.h>

graphics::Sprite::Sprite()
	: m_currentPosition(sf::Vector2f(0.0f, 0.0f)), m_previousPosition(sf::Vector2f(0.0f, 0.0f)), m_layer(1)
{
}

graphics::Sprite::Sprite(const sf::Vector2f& initialPosition, const sf::Texture& texture)
	: m_currentPosition(initialPosition), m_previousPosition(initialPosition)
{
	this->setTexture(texture);
}


sf::Vector2f graphics::Sprite::getCurrentPosition()
{
	return m_currentPosition;
}

void graphics::Sprite::setCurrentPosition(const sf::Vector2f& currentPosition)
{
	m_currentPosition = currentPosition;
}

sf::Vector2f graphics::Sprite::getPreviousPosition()
{
	return m_previousPosition;
}

void graphics::Sprite::setPreviousPosition(const sf::Vector2f& previousPosition)
{
	m_previousPosition = previousPosition;
}

int graphics::Sprite::getLayer()
{
	return m_layer;
}

void graphics::Sprite::setLayer(int layer)
{
	m_layer = layer;
}
