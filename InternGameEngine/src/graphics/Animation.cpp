#include <graphics/Animation.h>

graphics::Animation::Animation()
	: m_timePerFrame(sf::seconds(0.02)), m_isLooped(false)
{
}



sf::Time graphics::Animation::getTimePerFrame()
{
	return m_timePerFrame;
}

void graphics::Animation::setTimePerFrame(const sf::Time & timePerFrame)
{
	m_timePerFrame = timePerFrame;
}

bool graphics::Animation::isLooped()
{
	return m_isLooped;
}

void graphics::Animation::setLooped(bool isLoop)
{
	m_isLooped = isLoop;
}

void graphics::Animation::setSpriteSheet(const sf::Texture& spriteSheet)
{
	m_spriteSheet = &(spriteSheet);
}

void graphics::Animation::addFrame(const sf::IntRect& rect)
{
	m_frames.push_back(rect);
}

const sf::Texture* graphics::Animation::getSpriteSheet() const
{
	return m_spriteSheet;
}

std::size_t graphics::Animation::getFrameCount()
{
	return m_frames.size();
}

sf::IntRect& graphics::Animation::getFrame(std::size_t n)
{
	return m_frames[n];
}
