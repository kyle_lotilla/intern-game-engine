#include <graphics/TextRenderSystem.h>
#include <physics/Transform.h>
#include <graphics/Text.h>

graphics::TextRenderSystem::TextRenderSystem(const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<ecs::EntityManager>& entityManager)
	: m_componentManager(componentManager), m_entityManager(entityManager)
{
}

void graphics::TextRenderSystem::render(float interpolation, sf::RenderTarget & renderTarget, int layer)
{
	const ecs::ComponentArray<Text>* textArray = m_componentManager->getComponentArray<Text>();
	if (textArray)
	{
		for (Text& text : *textArray)
		{
			if (text.getLayer() == layer)
			{
				ecs::Entity* entity = m_entityManager->getEntity(text.getEntityID());
				if (entity)
				{
					physics::Transform& transform = entity->getTransform();
					text.setPreviousPosition(text.getCurrentPosition());
					text.setCurrentPosition(transform.getPosition() + text.getLocalPosition());
					text.setOrigin(text.getLocalBounds().left, text.getLocalBounds().top);

					text.setPosition(text.getCurrentPosition() * interpolation + text.getPreviousPosition() * (1.0f - interpolation));
					renderTarget.draw(text);
				}
			}
		}
	}
}
