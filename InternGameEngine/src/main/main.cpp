#include <engine/GameApp.h>
#include <gameplay/LevelScene.h>
#include <gameplay/MainMenuScene.h>

int main()
{
	engine::GameApp game;
	gameplay::LevelScene levelScene(&game.getWindow());
	gameplay::MainMenuScene mainMenuScene(&game.getWindow());
	game.addScene<gameplay::LevelScene>("LEVEL", levelScene);
	game.addScene<gameplay::MainMenuScene>("MAIN_MENU", mainMenuScene);
	game.loadScene("MAIN_MENU");
	game.run();
	return 0;
}