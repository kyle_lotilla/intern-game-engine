#include <gameplay/BossPhaseTransitionSystem.h>
#include <gameplay/Health.h>

gameplay::BossPhaseTransitionSystem::BossPhaseTransitionSystem(int bossID, const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<events::EventManager>& eventManager)
	: m_bossID(bossID), m_componentManager(componentManager), m_eventManager(eventManager), m_isPhaseTwo(false)
{
}

void gameplay::BossPhaseTransitionSystem::update(sf::Time deltaTime) const
{
	if (!m_isPhaseTwo)
	{
		Health* health = m_componentManager->getComponent<Health>(m_bossID);
		if (health)
		{
			if (health->getHealthPercent() <= 0.50f)
			{
				m_isPhaseTwo = true;
				m_eventManager->fireEvent("PHASE_TWO");
			}
		}
	}	
}
