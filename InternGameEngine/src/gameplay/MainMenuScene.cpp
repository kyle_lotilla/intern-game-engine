#include <gameplay/MainMenuScene.h>
#include <graphics/Sprite.h>
#include <graphics/Text.h>
#include <ui/MouseUIButton.h>
#include <engine/EngineRequestComponent.h>
#include <graphics/SpriteRenderSystem.h>
#include <graphics/TextRenderSystem.h>
#include <ui/MouseUIButtonSystem.h>
#include <gameplay/MainMenuSystem.h>

gameplay::MainMenuScene::MainMenuScene(const sf::RenderWindow * window) 
	: m_window(window)
{
}

void gameplay::MainMenuScene::loadScene(util::ArgsObject loadArgs)
{
	initializeAllPtr();
	registerComponents();

	engine::EngineRequestComponent engineRequestComponent;
	int backgroundID = m_entityManager->createEntity();
	m_componentManager->insertComponent<engine::EngineRequestComponent>(backgroundID, engineRequestComponent);

	int titleID = m_entityManager->createEntity(physics::Transform(sf::Vector2f(130.0f, 80.0f)));
	int playGameID = m_entityManager->createEntity(physics::Transform(sf::Vector2f(720.0f, 520.0f)));
	int quitGameID = m_entityManager->createEntity(physics::Transform(sf::Vector2f(300.0f, 520.0f)));

	m_musicManager->addMusic("BACKGROUND", "resources/music/Komiku_-_07_-_Run_against_the_universe.wav");
	m_musicManager->playMusic("BACKGROUND");


	sf::Texture backgroundTexture;
	if (backgroundTexture.loadFromFile("resources/background/background.png"))
	{
		m_textureResourceManager->addResource("BACKGROUND", backgroundTexture);
		graphics::Sprite backgroundSprite;
		backgroundSprite.setTexture(*(m_textureResourceManager->getResource("BACKGROUND")));
		m_componentManager->insertComponent<graphics::Sprite>(backgroundID, backgroundSprite);
	}

	

	sf::Font headerFont;
	if (headerFont.loadFromFile("resources/ui/Quantum.otf"))
	{
		m_fontResourceManager->addResource("HEADER", headerFont);
		graphics::Text titleText;
		titleText.setFont(*m_fontResourceManager->getResource("HEADER"));
		titleText.setCharacterSize(100);
		titleText.setString("GIANTS OF SPACE");
		titleText.setFillColor(sf::Color(190, 20, 20));
		m_componentManager->insertComponent<graphics::Text>(titleID, titleText);
	}

	sf::Font uiFont;
	if (uiFont.loadFromFile("resources/ui/Gamer.ttf"))
	{
		m_fontResourceManager->addResource("UI", uiFont);

		graphics::Text uiText;
		uiText.setFont(*m_fontResourceManager->getResource("UI"));
		uiText.setCharacterSize(64);

		uiText.setString("Play Game");
		m_componentManager->insertComponent<graphics::Text>(playGameID, uiText);
		ui::MouseUIButton playGameButton("PLAY_GAME", physics::BoundingBox(sf::Vector2f(0.0f, 0.0f), sf::Vector2f(uiText.getGlobalBounds().width, uiText.getGlobalBounds().height)));
		m_componentManager->insertComponent<ui::MouseUIButton>(playGameID, playGameButton);

		uiText.setString("Quit Game");
		m_componentManager->insertComponent<graphics::Text>(quitGameID, uiText);
		ui::MouseUIButton quitGameButton("QUIT_GAME", physics::BoundingBox(sf::Vector2f(0.0f, 0.0f), sf::Vector2f(uiText.getGlobalBounds().width, uiText.getGlobalBounds().height)));
		m_componentManager->insertComponent<ui::MouseUIButton>(quitGameID, quitGameButton);
	}
	
	input::InputContext<sf::Mouse::Button, input::MouseButtonAction> menuContext("MAIN_MENU");
	menuContext.addInputActionMapping(sf::Mouse::Left, input::MouseButtonAction("LEFT_CLICK"));
	m_mouseButtonInputHandler->addInputContext(menuContext);
	m_inputManager->addInputHandler<input::MouseButtonInputHandler>(m_mouseButtonInputHandler);
	m_inputManager->setCurrentContext("MAIN_MENU");

	loadSystems();
}

void gameplay::MainMenuScene::pollInput()
{
	m_inputManager->pollInput();
}

void gameplay::MainMenuScene::update(const sf::Time & deltaTime)
{
	m_systemManager.update(deltaTime);

	const ecs::ComponentArray<engine::EngineRequestComponent>* engineRequestArray = m_componentManager->getComponentArray<engine::EngineRequestComponent>();
	if (engineRequestArray)
	{
		for (engine::EngineRequestComponent& engineRequestComponent : *engineRequestArray)
		{
			if (engineRequestComponent.engineRequest == "PLAY_GAME")
			{
				util::ArgsObject loadArgs;
				loadArgs.setStringArg("LEVEL_FILE_LOCATION", "levels/level_1/level_1.json");
				requestLoadScene("LEVEL", loadArgs);
			}
			else if (engineRequestComponent.engineRequest == "QUIT_GAME")
			{
				requestQuitGame();
			}
		}
	}
}

void gameplay::MainMenuScene::render(float interpolation, sf::RenderTarget & renderTarget)
{
	m_renderSystemManager.render(interpolation, renderTarget);
}

void gameplay::MainMenuScene::unloadScene()
{
	m_musicManager->stopMusic();
	m_fontResourceManager->clear();
	m_textureResourceManager->clear();
	m_componentManager->reset();
	m_inputManager->clear();
	m_mouseButtonInputHandler->clear();
	m_systemManager.clear();
	m_renderSystemManager.clear();
}

void gameplay::MainMenuScene::initializeAllPtr()
{
	m_componentManager = std::make_shared<ecs::ComponentManager>();
	m_entityManager = std::make_shared<ecs::EntityManager>(m_componentManager);
	m_inputManager = std::make_shared<input::InputManager>();
	m_mouseButtonInputHandler = std::make_shared<input::MouseButtonInputHandler>(m_window);
	m_textureResourceManager = std::make_shared<util::ResourceManager<sf::Texture>>();
	m_fontResourceManager = std::make_shared<util::ResourceManager<sf::Font>>();
	m_musicManager = std::make_shared<audio::MusicManager>();
}

void gameplay::MainMenuScene::registerComponents()
{
	m_componentManager->registerComponent<graphics::Sprite>();
	m_componentManager->registerComponent<graphics::Text>();
	m_componentManager->registerComponent<ui::MouseUIButton>();
	m_componentManager->registerComponent<engine::EngineRequestComponent>();
}

void gameplay::MainMenuScene::loadSystems()
{
	std::shared_ptr<graphics::SpriteRenderSystem> spriteSystem = std::make_shared<graphics::SpriteRenderSystem>(m_componentManager, m_entityManager);
	m_renderSystemManager.addSystem<graphics::SpriteRenderSystem>(spriteSystem);

	std::shared_ptr<graphics::TextRenderSystem> textRenderSystem = std::make_shared<graphics::TextRenderSystem>(m_componentManager, m_entityManager);
	m_renderSystemManager.addSystem<graphics::TextRenderSystem>(textRenderSystem);

	std::shared_ptr<ui::MouseUIButtonSystem> mouseUIButtionSystem = std::make_shared<ui::MouseUIButtonSystem>(m_entityManager, m_componentManager, m_mouseButtonInputHandler);
	m_systemManager.addSystem<ui::MouseUIButtonSystem>(mouseUIButtionSystem);

	std::shared_ptr<gameplay::MainMenuSystem> mainMenuSystem = std::make_shared<gameplay::MainMenuSystem>(m_componentManager);
	m_systemManager.addSystem<gameplay::MainMenuSystem>(mainMenuSystem);
}
