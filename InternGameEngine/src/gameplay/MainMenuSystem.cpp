#include <gameplay/MainMenuSystem.h>
#include <ui/MouseUIButton.h>
#include <engine/EngineRequestComponent.h>

gameplay::MainMenuSystem::MainMenuSystem(const std::shared_ptr<ecs::ComponentManager>& componentManager)
	: m_componentManager(componentManager)
{
}

void gameplay::MainMenuSystem::update(sf::Time deltaTime) const
{
	const ecs::ComponentArray<ui::MouseUIButton>* mouseUIButtonArray = m_componentManager->getComponentArray<ui::MouseUIButton>();

	if (mouseUIButtonArray)
	{
		for (ui::MouseUIButton& mouseUIButton : *mouseUIButtonArray)
		{
			if (mouseUIButton.isPressed())
			{
				if (mouseUIButton.getButtonName() == "PLAY_GAME")
				{
					const ecs::ComponentArray<engine::EngineRequestComponent>* engineRequestComponentArray = m_componentManager->getComponentArray<engine::EngineRequestComponent>();

					if (engineRequestComponentArray)
					{
						for (engine::EngineRequestComponent& engineRequestComponent : *engineRequestComponentArray)
						{
							engineRequestComponent.engineRequest = "PLAY_GAME";
						}
					}
				}
				else if (mouseUIButton.getButtonName() == "QUIT_GAME")
				{
					const ecs::ComponentArray<engine::EngineRequestComponent>* engineRequestComponentArray = m_componentManager->getComponentArray<engine::EngineRequestComponent>();

					if (engineRequestComponentArray)
					{
						for (engine::EngineRequestComponent& engineRequestComponent : *engineRequestComponentArray)
						{
							engineRequestComponent.engineRequest = "QUIT_GAME";
						}
					}
				}
			}
		}
	}
}
