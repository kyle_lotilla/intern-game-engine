#include <gameplay/ShowGameOverSystem.h>
#include <graphics/Text.h>


gameplay::ShowGameOverSystem::ShowGameOverSystem(std::string nextLevel, std::shared_ptr<ecs::EntityManager>& entityManager, const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<input::InputManager>& inputManager, const std::shared_ptr<audio::MusicManager>& musicManager)
	: m_entityManager(entityManager), m_componentManager(componentManager), m_inputManager(inputManager), m_musicManager(musicManager)
{
	if (nextLevel == "NONE")
	{
		m_isThereNextLevel = false;
	}
	else
	{
		m_isThereNextLevel = true;
	}
}

void gameplay::ShowGameOverSystem::eventTriggered(events::Event event)
{
	if (event.getEventName() == "PLAYER_DEATH")
	{
		m_inputManager->setCurrentContext("GAME_OVER_MENU");
		m_entityManager->createEntity("BLACK_OVERLAY");

		int headerEntityID = m_entityManager->createEntity("HEADER", physics::Transform(sf::Vector2f(230.0f, 80.0f)));
		graphics::Text* headerText = m_componentManager->getComponent<graphics::Text>(headerEntityID);

		if (headerText)
		{
			headerText->setString("YOU LOSE");
		}

		m_entityManager->createEntity("MAIN_MENU_BUTTON", physics::Transform(sf::Vector2f(290.0f, 520.0f)));
		m_entityManager->createEntity("REPLAY_BUTTON", physics::Transform(sf::Vector2f(750.0f, 520.0f)));

		m_musicManager->playMusic("LOSE");
	}
	else if (event.getEventName() == "BOSS_DEATH")
	{
		m_inputManager->setCurrentContext("GAME_OVER_MENU");
		m_entityManager->createEntity("BLACK_OVERLAY");

		int headerEntityID = m_entityManager->createEntity("HEADER", physics::Transform(sf::Vector2f(300.0f, 80.0f)));
		graphics::Text* headerText = m_componentManager->getComponent<graphics::Text>(headerEntityID);

		if (headerText)
		{
			headerText->setString("YOU WIN!");
		}

		m_entityManager->createEntity("MAIN_MENU_BUTTON", physics::Transform(sf::Vector2f(290.0f, 520.0f)));

		if (m_isThereNextLevel)
		{
			m_entityManager->createEntity("NEXT_LEVEL_BUTTON", physics::Transform(sf::Vector2f(700.0f, 520.0f)));
		}

		m_musicManager->playMusic("WIN");
	}
}
