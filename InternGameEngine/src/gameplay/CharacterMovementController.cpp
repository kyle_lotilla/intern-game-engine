#include <gameplay/CharacterMovementController.h>

gameplay::CharacterMovementController::CharacterMovementController(const sf::Vector2f & characterVelocity, const sf::Vector2f & boundariesX, const sf::Vector2f & boundariesY)
	: m_characterVelocity(characterVelocity), m_boundariesX(boundariesX), m_boundariesY(boundariesY)
{
}

sf::Vector2f gameplay::CharacterMovementController::getCharacterVelocity()
{
	return m_characterVelocity;
}

void gameplay::CharacterMovementController::setCharacterVelocity(const sf::Vector2f & characterVelocity)
{
	m_characterVelocity = characterVelocity;
}

sf::Vector2f gameplay::CharacterMovementController::getBoundariesX()
{
	return m_boundariesX;
}

sf::Vector2f gameplay::CharacterMovementController::getBoundariesY()
{
	return m_boundariesY;
}
