#include <gameplay/LevelScene.h>



gameplay::LevelScene::LevelScene(const sf::RenderWindow* window)
	: m_window(window), m_componentManager(nullptr), m_entityManager(nullptr),
	m_inputManager(nullptr), m_keyboardInputHandler(nullptr), m_mouseButtonInputHandler(nullptr),
	m_textureResourceManager(nullptr), m_fontResourceManager(nullptr), m_soundBufferResourceManager(nullptr),
	m_eventManager(nullptr), m_musicManager(nullptr)
{
}

void gameplay::LevelScene::loadScene(util::ArgsObject loadArgs)
{
	initializeAllPtr();
	registerComponents();
	loadLevel(*loadArgs.getStringArg("LEVEL_FILE_LOCATION"));
}

void gameplay::LevelScene::pollInput()
{
	m_inputManager->pollInput();
}

void gameplay::LevelScene::update(const sf::Time & deltaTime)
{
	m_systemManager.update(deltaTime);

	const ecs::ComponentArray<engine::EngineRequestComponent>* engineRequestArray = m_componentManager->getComponentArray<engine::EngineRequestComponent>();
	if (engineRequestArray)
	{
		for (engine::EngineRequestComponent& engineRequestComponent : *engineRequestArray)
		{
			if (engineRequestComponent.engineRequest == "REPLAY")
			{
				util::ArgsObject loadArgs;
				loadArgs.setStringArg("LEVEL_FILE_LOCATION", m_currentLevelLocation);
				requestLoadScene("LEVEL", loadArgs);
			}
			else if (engineRequestComponent.engineRequest == "MAIN_MENU")
			{
				requestLoadScene("MAIN_MENU");
			}
			else if (engineRequestComponent.engineRequest == "NEXT_LEVEL")
			{
				util::ArgsObject loadArgs;
				loadArgs.setStringArg("LEVEL_FILE_LOCATION", m_nextLevelLocation);
				requestLoadScene("LEVEL", loadArgs);
			}
		}
	}
}

void gameplay::LevelScene::render(float interpolation, sf::RenderTarget & renderTarget)
{
	m_renderSystemManager.render(interpolation, renderTarget);
}

void gameplay::LevelScene::unloadScene()
{
	m_componentManager->reset();
	m_eventManager->clear();
	m_fontResourceManager->clear();
	m_soundBufferResourceManager->clear();
	m_textureResourceManager->clear();
	m_musicManager->clear();
	m_keyboardInputHandler->clear();
	m_mouseButtonInputHandler->clear();
	m_inputManager->clear();
	m_systemManager.clear();
	m_renderSystemManager.clear();
}

void gameplay::LevelScene::initializeAllPtr()
{
	m_componentManager = std::make_shared<ecs::ComponentManager>();
	m_entityManager = std::make_shared<ecs::EntityManager>(m_componentManager);
	m_inputManager = std::make_shared<input::InputManager>();
	m_textureResourceManager = std::make_shared<util::ResourceManager<sf::Texture>>();
	m_fontResourceManager = std::make_shared<util::ResourceManager<sf::Font>>();
	m_eventManager = std::make_shared<events::EventManager>();
	m_soundBufferResourceManager = std::make_shared<util::ResourceManager<sf::SoundBuffer>>(); 
	m_musicManager = std::make_shared<audio::MusicManager>();
	m_mouseButtonInputHandler = std::make_shared<input::MouseButtonInputHandler>(m_window);
	m_keyboardInputHandler = std::make_shared<input::KeyboardInputHandler>();
}

void gameplay::LevelScene::registerComponents()
{
	m_componentManager->registerComponent<graphics::Sprite>();
	m_componentManager->registerComponent<graphics::AnimationController>();
	m_componentManager->registerComponent<graphics::Text>();
	m_componentManager->registerComponent<physics::Collider>();
	m_componentManager->registerComponent<physics::RigidBody2D>();
	m_componentManager->registerComponent<gameplay::Health>();
	m_componentManager->registerComponent<gameplay::CharacterMovementController>();
	m_componentManager->registerComponent<gameplay::ProjectileShooter>();
	m_componentManager->registerComponent<gameplay::Damage>();
	m_componentManager->registerComponent<audio::AudioSource>();
	m_componentManager->registerComponent<ui::MouseUIButton>();
	m_componentManager->registerComponent<engine::EngineRequestComponent>();
}

void gameplay::LevelScene::loadLevel(std::string levelFileLocation)
{
	std::ifstream levelFileStream("resources/" + levelFileLocation);
	Json levelJson;
	levelFileStream >> levelJson;
	levelFileStream.close();

	int backgroundEntityID = m_entityManager->createEntity();
	graphics::Sprite backgroundSprite = loadSprite(levelJson["background"]);
	m_componentManager->insertComponent<graphics::Sprite>(backgroundEntityID, backgroundSprite);
	engine::EngineRequestComponent engineRequestComponent;
	m_componentManager->insertComponent<engine::EngineRequestComponent>(backgroundEntityID, engineRequestComponent);


	for (const Json& musicJson : levelJson["music"])
	{
		loadMusic(musicJson);
	}
	m_musicManager->playMusic("GAMEPLAY_MUSIC");

	m_playerID = loadCharacter(levelJson["player"]);
	m_bossID = loadCharacter(levelJson["boss"]);
	loadUI(levelJson["ui"].get<std::string>());
	loadInput(levelJson["input"].get<std::string>());
	m_nextLevelLocation = levelJson["nextLevelLocation"].get<std::string>();
	m_currentLevelLocation = levelFileLocation;
	loadSystems();
}

int gameplay::LevelScene::loadCharacter(const Json& characterJson)
{
	int characterID;

	if (characterJson.contains("transform"))
	{
		characterID = m_entityManager->createEntity(loadTransform(characterJson["transform"]));
	}
	else
	{
		characterID = m_entityManager->createEntity();
	}
	
	if (characterJson.contains("dataFileLocation"))
	{
		std::ifstream characterDataStream("resources/" + characterJson["dataFileLocation"].get<std::string>());
		Json characterDataJson;
		characterDataStream >> characterDataJson;
		characterDataStream.close();

		if (characterDataJson.contains("animationController"))
		{
			graphics::AnimationController animationController = loadAnimationController(characterDataJson["animationController"]);
			animationController.setCurrentAnimation("NEUTRAL");
			m_componentManager->insertComponent<graphics::AnimationController>(characterID, animationController);
		}

		if (characterDataJson.contains("collider"))
		{
			physics::Collider collider = loadCollider(characterDataJson["collider"]);
			m_componentManager->insertComponent<physics::Collider>(characterID, collider);
		}

		if (characterDataJson.contains("health"))
		{
			gameplay::Health health(characterDataJson["health"].get<int>());
			m_componentManager->insertComponent<gameplay::Health>(characterID, health);
		}

		if (characterDataJson.contains("damage"))
		{
			gameplay::Damage damage(characterDataJson["damage"].get<int>());
			m_componentManager->insertComponent<gameplay::Damage>(characterID, damage);
		}

		if (characterDataJson.contains("characterMovementController"))
		{
			gameplay::CharacterMovementController characterMovementController = loadCharacterMovementController(characterDataJson["characterMovementController"]);
			m_componentManager->insertComponent<gameplay::CharacterMovementController>(characterID, characterMovementController);
		}

		if (characterDataJson.contains("audioSource"))
		{
			audio::AudioSource audioSource = loadAudioSource(characterDataJson["audioSource"]);
			m_componentManager->insertComponent<audio::AudioSource>(characterID, audioSource);
		}

		if (characterDataJson.contains("projectileShooter"))
		{
			gameplay::ProjectileShooter projectileShooter = loadProjectileShooter(characterDataJson["projectileShooter"]);
			m_componentManager->insertComponent<gameplay::ProjectileShooter>(characterID, projectileShooter);
		}

		physics::RigidBody2D rigidBody2D;
		m_componentManager->insertComponent<physics::RigidBody2D>(characterID, rigidBody2D);
	}

	return characterID;
}

void gameplay::LevelScene::loadProjectile(std::string projectileFileLocation)
{
	std::ifstream projectileDataStream("resources/" + projectileFileLocation);
	Json projectileDataJson;
	projectileDataStream >> projectileDataJson;
	projectileDataStream.close();

	if (projectileDataJson.contains("animationController"))
	{
		graphics::AnimationController animationController = loadAnimationController(projectileDataJson["animationController"]);
		animationController.setCurrentAnimation("NEUTRAL");
		m_componentManager->registerPrefabComponent<graphics::AnimationController>(projectileFileLocation, animationController);
	}
	else if (projectileDataJson.contains("sprite"))
	{
		graphics::Sprite sprite = loadSprite(projectileDataJson["sprite"]);
		m_componentManager->registerPrefabComponent<graphics::Sprite>(projectileFileLocation, sprite);
	}

	if (projectileDataJson.contains("collider"))
	{
		physics::Collider collider = loadCollider(projectileDataJson["collider"]);
		m_componentManager->registerPrefabComponent<physics::Collider>(projectileFileLocation, collider);
	}

	if (projectileDataJson.contains("rigidBody2D"))
	{
		physics::RigidBody2D rigidBody = loadRigidBody2D(projectileDataJson["rigidBody2D"]);
		m_componentManager->registerPrefabComponent<physics::RigidBody2D>(projectileFileLocation, rigidBody);
	}

	if (projectileDataJson.contains("damage"))
	{
		gameplay::Damage damage = gameplay::Damage(projectileDataJson["damage"].get<int>());
		m_componentManager->registerPrefabComponent<gameplay::Damage>(projectileFileLocation, damage);
	}

}

void gameplay::LevelScene::loadUI(std::string uiFileLocation)
{
	graphics::Sprite blackOverlaySprite;
	sf::Texture blackOverlayTexture;
	if (blackOverlayTexture.loadFromFile("resources/ui/overlay.png"))
	{
		m_textureResourceManager->addResource("OVERLAY", blackOverlayTexture);
		blackOverlaySprite.setTexture(*(m_textureResourceManager->getResource("OVERLAY")));
	}
	blackOverlaySprite.setColor(sf::Color(0, 0, 0, 128));
	blackOverlaySprite.setLayer(3);
	m_componentManager->registerPrefabComponent<graphics::Sprite>("BLACK_OVERLAY", blackOverlaySprite);

	std::ifstream uiFileStream("resources/" + uiFileLocation);
	Json uiJson;
	uiFileStream >> uiJson;
	uiFileStream.close();

	if (uiJson.contains("healthUI"))
	{
		if (uiJson["healthUI"].contains("transform"))
		{
			m_healthUIID = m_entityManager->createEntity(loadTransform(uiJson["healthUI"]["transform"]));
		}
		else
		{
			m_healthUIID = m_entityManager->createEntity();
		}

		if (uiJson["healthUI"].contains("text"))
		{
			graphics::Text text = loadText(uiJson["healthUI"]["text"]);
			m_componentManager->insertComponent<graphics::Text>(m_healthUIID, text);
		}

		if (uiJson["healthUI"].contains("sprite"))
		{
			graphics::Sprite sprite = loadSprite(uiJson["healthUI"]["sprite"]);
			m_componentManager->insertComponent<graphics::Sprite>(m_healthUIID, sprite);
		}
	}

	if (uiJson.contains("headerText"))
	{
		graphics::Text text = loadText(uiJson["headerText"]);
		m_componentManager->registerPrefabComponent<graphics::Text>("HEADER", text);
	}

	if (uiJson.contains("uiText"))
	{
		graphics::Text uiText = loadText(uiJson["uiText"]);

		uiText.setString("Main Menu");
		m_componentManager->registerPrefabComponent<graphics::Text>("MAIN_MENU_BUTTON", uiText);
		ui::MouseUIButton mainMenuButton("MAIN_MENU", physics::BoundingBox(sf::Vector2f(0.0f, 0.0f), sf::Vector2f(uiText.getGlobalBounds().width, uiText.getGlobalBounds().height)));
		m_componentManager->registerPrefabComponent<ui::MouseUIButton>("MAIN_MENU_BUTTON", mainMenuButton);

		uiText.setString("Replay");
		m_componentManager->registerPrefabComponent<graphics::Text>("REPLAY_BUTTON", uiText);
		ui::MouseUIButton replayButton("REPLAY", physics::BoundingBox(sf::Vector2f(0.0f, 0.0f), sf::Vector2f(uiText.getGlobalBounds().width, uiText.getGlobalBounds().height)));
		m_componentManager->registerPrefabComponent<ui::MouseUIButton>("REPLAY_BUTTON", replayButton);

		uiText.setString("Next Level");
		m_componentManager->registerPrefabComponent<graphics::Text>("NEXT_LEVEL_BUTTON", uiText);
		ui::MouseUIButton nextLevelButton("NEXT_LEVEL", physics::BoundingBox(sf::Vector2f(0.0f, 0.0f), sf::Vector2f(uiText.getGlobalBounds().width, uiText.getGlobalBounds().height)));
		m_componentManager->registerPrefabComponent<ui::MouseUIButton>("NEXT_LEVEL_BUTTON", nextLevelButton);
	}
}

void gameplay::LevelScene::loadInput(std::string inputFileLocation)
{
	std::ifstream inputFileStream("resources/" + inputFileLocation);
	Json inputJson;
	inputFileStream >> inputJson;
	inputFileStream.close();

	input::InputContext<sf::Keyboard::Key, input::ButtonAction> gameplayContext("GAMEPLAY");

	if (inputJson.contains("UP"))
	{
		gameplayContext.addInputActionMapping(thor::toKeyboardKey(inputJson["UP"].get<std::string>()), input::ButtonAction("UP"));
	}

	if (inputJson.contains("DOWN"))
	{
		gameplayContext.addInputActionMapping(thor::toKeyboardKey(inputJson["DOWN"].get<std::string>()), input::ButtonAction("DOWN"));
	}

	if (inputJson.contains("LEFT"))
	{
		gameplayContext.addInputActionMapping(thor::toKeyboardKey(inputJson["LEFT"].get<std::string>()), input::ButtonAction("LEFT"));
	}

	if (inputJson.contains("RIGHT"))
	{
		gameplayContext.addInputActionMapping(thor::toKeyboardKey(inputJson["RIGHT"].get<std::string>()), input::ButtonAction("RIGHT"));
	}

	if (inputJson.contains("SHOOT"))
	{
		gameplayContext.addInputActionMapping(thor::toKeyboardKey(inputJson["SHOOT"].get<std::string>()), input::ButtonAction("SHOOT"));
	}

	input::InputContext<sf::Mouse::Button, input::MouseButtonAction> menuContext("GAME_OVER_MENU");
	menuContext.addInputActionMapping(sf::Mouse::Left, input::MouseButtonAction("LEFT_CLICK"));

	m_keyboardInputHandler->addInputContext(gameplayContext);
	m_mouseButtonInputHandler->addInputContext(menuContext);

	m_inputManager->addInputHandler<input::KeyboardInputHandler>(m_keyboardInputHandler);
	m_inputManager->addInputHandler<input::MouseButtonInputHandler>(m_mouseButtonInputHandler);
	m_inputManager->setCurrentContext("GAMEPLAY");
}

void gameplay::LevelScene::loadSystems()
{
	std::shared_ptr<graphics::TextRenderSystem> textRenderSystem = std::make_shared<graphics::TextRenderSystem>(m_componentManager, m_entityManager);
	m_renderSystemManager.addSystem<graphics::TextRenderSystem>(textRenderSystem);

	std::shared_ptr<graphics::SpriteRenderSystem> spriteSystem = std::make_shared<graphics::SpriteRenderSystem>(m_componentManager, m_entityManager);
	m_renderSystemManager.addSystem<graphics::SpriteRenderSystem>(spriteSystem);

	std::shared_ptr<graphics::AnimationControllerRenderSystem> animationControllerRenderSystem = std::make_shared<graphics::AnimationControllerRenderSystem>(m_componentManager, m_entityManager);
	m_renderSystemManager.addSystem<graphics::AnimationControllerRenderSystem>(animationControllerRenderSystem);

	std::shared_ptr<gameplay::PlayerMovementSystem> movementSystem = std::make_shared<gameplay::PlayerMovementSystem>(m_playerID, m_entityManager, m_componentManager, m_keyboardInputHandler);
	m_systemManager.addSystem<gameplay::PlayerMovementSystem>(movementSystem);

	std::shared_ptr<gameplay::BossMovementSystem> bossMovementSystem = std::make_shared<gameplay::BossMovementSystem>(m_bossID, m_entityManager, m_componentManager);
	m_systemManager.addSystem<gameplay::BossMovementSystem>(bossMovementSystem);

	std::shared_ptr<physics::RigidBodyVelocitySystem> velocitySystem = std::make_shared<physics::RigidBodyVelocitySystem>(m_componentManager, m_entityManager);
	m_systemManager.addSystem<physics::RigidBodyVelocitySystem>(velocitySystem);

	std::shared_ptr<physics::ColliderMovementSystem> colliderMovementSystem = std::make_shared<physics::ColliderMovementSystem>(m_entityManager, m_componentManager);
	m_systemManager.addSystem<physics::ColliderMovementSystem>(colliderMovementSystem);

	std::shared_ptr<physics::CollisionSystem> collisionSystem = std::make_shared< physics::CollisionSystem>(m_componentManager);
	m_systemManager.addSystem<physics::CollisionSystem>(collisionSystem);

	std::shared_ptr<gameplay::PlayerCollisionDamageSystem> playerCollisionDamageSystem = std::make_shared<gameplay::PlayerCollisionDamageSystem>(m_playerID, m_componentManager, m_eventManager);
	m_systemManager.addSystem<gameplay::PlayerCollisionDamageSystem>(playerCollisionDamageSystem);

	std::shared_ptr<gameplay::PlayerHealthTextSystem> playerHealthTextSystem = std::make_shared<gameplay::PlayerHealthTextSystem>(m_playerID, m_healthUIID, m_componentManager);
	m_systemManager.addSystem<gameplay::PlayerHealthTextSystem>(playerHealthTextSystem);
	m_eventManager->addEvent("PLAYER_DAMAGED");
	m_eventManager->registerToEvent("PLAYER_DAMAGED", playerHealthTextSystem);

	std::shared_ptr<gameplay::PlayerDeathSystem> playerDeathSystem = std::make_shared<gameplay::PlayerDeathSystem>(m_playerID, m_componentManager, m_eventManager);
	m_systemManager.addSystem<gameplay::PlayerDeathSystem>(playerDeathSystem);

	std::shared_ptr<gameplay::BossCollisionDamageSystem> bossCollisionDamageSystem = std::make_shared<gameplay::BossCollisionDamageSystem>(m_bossID, m_componentManager);
	m_systemManager.addSystem<gameplay::BossCollisionDamageSystem>(bossCollisionDamageSystem);

	std::shared_ptr<gameplay::BossDeathSystem> bossDeathSystem = std::make_shared<gameplay::BossDeathSystem>(m_bossID, m_componentManager, m_eventManager);
	m_systemManager.addSystem<gameplay::BossDeathSystem>(bossDeathSystem);

	std::vector<int> laserEntityPool = m_entityManager->createEntities(20);
	std::shared_ptr<gameplay::PlayerProjectileSystem> playerProjectileSystem = std::make_shared<gameplay::PlayerProjectileSystem>(m_playerID, laserEntityPool, m_entityManager, m_componentManager, m_keyboardInputHandler);
	m_systemManager.addSystem<gameplay::PlayerProjectileSystem>(playerProjectileSystem);

	std::vector<int> bossProjectilePool = m_entityManager->createEntities(50);
	std::shared_ptr<gameplay::BossProjectileSystem> bossProjectileSystem = std::make_shared<gameplay::BossProjectileSystem>(m_bossID, bossProjectilePool, m_entityManager, m_componentManager);
	m_systemManager.addSystem<gameplay::BossProjectileSystem>(bossProjectileSystem);

	std::shared_ptr<graphics::AnimationSystem> animationSystem = std::make_shared<graphics::AnimationSystem>(m_componentManager);
	m_systemManager.addSystem<graphics::AnimationSystem>(animationSystem);

	std::shared_ptr<gameplay::BossPhaseTransitionSystem> bossPhaseTransitionSystem = std::make_shared<gameplay::BossPhaseTransitionSystem>(m_bossID, m_componentManager, m_eventManager);
	m_systemManager.addSystem<gameplay::BossPhaseTransitionSystem>(bossPhaseTransitionSystem);

	std::shared_ptr<gameplay::BossPhaseSpeedTransitionSystem> bossPhaseSpeedTransitionSystem = std::make_shared<gameplay::BossPhaseSpeedTransitionSystem>(m_bossID, m_componentManager);
	m_eventManager->addEvent("PHASE_TWO");
	m_eventManager->registerToEvent<gameplay::BossPhaseSpeedTransitionSystem>("PHASE_TWO", bossPhaseSpeedTransitionSystem);
	m_eventManager->registerToEvent<gameplay::BossCollisionDamageSystem>("PHASE_TWO", bossCollisionDamageSystem);

	std::shared_ptr<ui::MouseUIButtonSystem> mouseUIButtionSystem = std::make_shared<ui::MouseUIButtonSystem>(m_entityManager, m_componentManager, m_mouseButtonInputHandler);
	m_systemManager.addSystem<ui::MouseUIButtonSystem>(mouseUIButtionSystem);

	std::shared_ptr<gameplay::GameOverMenuSystem> gameOverMenuSystem = std::make_shared<gameplay::GameOverMenuSystem>(m_componentManager);
	m_systemManager.addSystem<gameplay::GameOverMenuSystem>(gameOverMenuSystem);

	std::shared_ptr<gameplay::ShowGameOverSystem> showGameOverSystem = std::make_shared<gameplay::ShowGameOverSystem>(m_nextLevelLocation, m_entityManager, m_componentManager, m_inputManager, m_musicManager);
	m_eventManager->addEvent("PLAYER_DEATH_START");
	m_eventManager->addEvent("PLAYER_DEATH");
	m_eventManager->addEvent("BOSS_DEATH");
	m_eventManager->registerToEvent("PLAYER_DEATH_START", bossDeathSystem);
	m_eventManager->registerToEvent("PLAYER_DEATH", showGameOverSystem);
	m_eventManager->registerToEvent("BOSS_DEATH", showGameOverSystem);

	m_systemManager.start();
}

graphics::Sprite gameplay::LevelScene::loadSprite(const Json& spriteJson)
{
	graphics::Sprite sprite;

	if (spriteJson.contains("texture"))
	{
		std::string textureFileLocation = spriteJson["texture"].get<std::string>();
		loadTexture(textureFileLocation);
		sprite.setTexture(*(m_textureResourceManager->getResource(textureFileLocation)));
	}

	if (spriteJson.contains("layer"))
	{
		sprite.setLayer(spriteJson["layer"].get<int>());
	}

	if (spriteJson.contains("scale"))
	{
		sf::Vector2f scale(1.0f, 1.0f);

		if (spriteJson["scale"].contains("x"))
		{
			scale.x = spriteJson["scale"]["x"].get<float>();
		}

		if (spriteJson["scale"].contains("y"))
		{
			scale.y = spriteJson["scale"]["y"].get<float>();
		}

		sprite.setScale(scale);
	}

	if (spriteJson.contains("color"))
	{
		sf::Color color;
		if (spriteJson["color"].contains("red"))
		{
			color.r = spriteJson["color"]["red"].get<int>();
		}

		if (spriteJson["color"].contains("green"))
		{
			color.g = spriteJson["color"]["green"].get<int>();
		}

		if (spriteJson["color"].contains("blue"))
		{
			color.b = spriteJson["color"]["blue"].get<int>();
		}

		if (spriteJson["color"].contains("alpha"))
		{
			color.a = spriteJson["color"]["alpha"].get<int>();
		}
		sprite.setColor(color);
	}

	if (spriteJson.contains("textureRect"))
	{
		sf::IntRect textureRect;
		if (spriteJson["textureRect"].contains("left"))
		{
			textureRect.left = spriteJson["textureRect"]["left"].get<int>();
		}

		if (spriteJson["textureRect"].contains("top"))
		{
			textureRect.top = spriteJson["textureRect"]["top"].get<int>();
		}

		if (spriteJson["textureRect"].contains("width"))
		{
			textureRect.width = spriteJson["textureRect"]["width"].get<int>();
		}

		if (spriteJson["textureRect"].contains("height"))
		{
			textureRect.height = spriteJson["textureRect"]["height"].get<int>();
		}

		sprite.setTextureRect(textureRect);
	}

	return sprite;
}

graphics::AnimationController gameplay::LevelScene::loadAnimationController(const Json & animationControllerJson)
{
	graphics::AnimationController animationController;

	if (animationControllerJson.contains("animations"))
	{
		for (const Json& animationJson : animationControllerJson["animations"])
		{
			if (animationJson.contains("name"))
			{
				animationController.addAnimation(animationJson["name"].get<std::string>(), loadAnimation(animationJson));
			}
		}
	}

	if (animationControllerJson.contains("looped"))
	{
		animationController.setLooped(animationControllerJson["looped"].get<bool>());
	}

	if (animationControllerJson.contains("frameTime"))
	{
		animationController.setTimePerFrame(sf::seconds(animationControllerJson["frameTime"].get<float>()));
	}

	if (animationControllerJson.contains("layer"))
	{
		animationController.setLayer(animationControllerJson["layer"].get<int>());
	}

	return animationController;
}

graphics::Animation gameplay::LevelScene::loadAnimation(const Json& animationJson)
{
	graphics::Animation animation;

	if (animationJson.contains("texture"))
	{
		std::string fileLocation = animationJson["texture"].get<std::string>();
		loadTexture(fileLocation);
		animation.setSpriteSheet(*(m_textureResourceManager->getResource(fileLocation)));
	}

	if (animationJson.contains("frames"))
	{
		for (const Json& frameJson : animationJson["frames"])
		{
			sf::IntRect frame(0,0,0,0);

			if (frameJson.contains("left"))
			{
				frame.left = frameJson["left"].get<int>();
			}

			if (frameJson.contains("top"))
			{
				frame.top = frameJson["top"].get<int>();
			}

			if (frameJson.contains("width"))
			{
				frame.width = frameJson["width"].get<int>();
			}

			if (frameJson.contains("height"))
			{
				frame.height = frameJson["height"].get<int>();
			}

			animation.addFrame(frame);
		}
	}

	if (animationJson.contains("looped"))
	{
		animation.setLooped(animationJson["looped"].get<bool>());
	}

	if (animationJson.contains("frameTime"))
	{
		animation.setTimePerFrame(sf::seconds(animationJson["frameTime"].get<float>()));
	}

	return animation;
}

void gameplay::LevelScene::loadTexture(std::string fileLocation)
{
	sf::Texture texture;
	if (!m_textureResourceManager->isResourceLoaded(fileLocation))
	{
		if (texture.loadFromFile("resources/" + fileLocation))
		{
			m_textureResourceManager->addResource(fileLocation, texture);
		}
	}
}

void gameplay::LevelScene::loadMusic(const Json& musicJson)
{
	if (musicJson.contains("name") && musicJson.contains("fileLocation"))
	{
		m_musicManager->addMusic(musicJson["name"].get<std::string>(), "resources/" + musicJson["fileLocation"].get<std::string>());
	}
}

physics::Transform gameplay::LevelScene::loadTransform(const Json & transformJson)
{
	sf::Vector2f position(0.0f, 0.0f);
	float rotation = 0.0f;
	sf::Vector2f scale(1.0f, 1.0f);

	if (transformJson.contains("position"))
	{
		if (transformJson["position"].contains("x"))
		{
			position.x = transformJson["position"]["x"].get<float>();
		}

		if (transformJson["position"].contains("y"))
		{
			position.y = transformJson["position"]["y"].get<float>();
		}
	}

	if (transformJson.contains("rotation"))
	{
		rotation = transformJson["rotation"];
	}

	if (transformJson.contains("scale"))
	{
		if (transformJson["scale"].contains("x"))
		{
			scale.x = transformJson["scale"]["x"].get<float>();
		}

		if (transformJson["scale"].contains("y"))
		{
			scale.y = transformJson["scale"]["y"].get<float>();
		}
	}

	return physics::Transform(position, rotation, scale);
}


physics::Collider gameplay::LevelScene::loadCollider(const Json& colliderJson)
{
	std::string tagName = "NONE";
	if (colliderJson.contains("tagName"))
	{
		tagName = colliderJson["tagName"].get<std::string>();
	}
	physics::Collider collider(tagName);

	if (colliderJson.contains("boundingBoxes"))
	{
		for (const Json& boundingBoxJson : colliderJson["boundingBoxes"])
		{
			collider.addBoundingBox(loadBoundingBox(boundingBoxJson));
		}
	}

	return collider;
}

physics::BoundingBox gameplay::LevelScene::loadBoundingBox(const Json & boundingBoxJson)
{
	float localLeft = 0.0f;
	float localTop = 0.0f;
	float width = 0.0f;
	float height = 0.0f;

	if (boundingBoxJson.contains("localLeft"))
	{
		localLeft = boundingBoxJson["localLeft"].get<float>();
	}

	if (boundingBoxJson.contains("localTop"))
	{
		localTop = boundingBoxJson["localTop"].get<float>();
	}

	if (boundingBoxJson.contains("width"))
	{
		width = boundingBoxJson["width"].get<float>();
	}

	if (boundingBoxJson.contains("height"))
	{
		height = boundingBoxJson["height"].get<float>();
	}

	return physics::BoundingBox(localLeft, localTop, width, height);
}

physics::RigidBody2D gameplay::LevelScene::loadRigidBody2D(const Json & rigidBody2DJson)
{
	physics::RigidBody2D rigidBody;

	if (rigidBody2DJson.contains("velocity"))
	{
		if (rigidBody2DJson["velocity"].contains("x"))
		{
			rigidBody.setVelocityX(rigidBody2DJson["velocity"]["x"].get<float>());
		}

		if (rigidBody2DJson["velocity"].contains("y"))
		{
			rigidBody.setVelocityY(rigidBody2DJson["velocity"]["y"].get<float>());
		}
	}

	return rigidBody;
}

gameplay::CharacterMovementController gameplay::LevelScene::loadCharacterMovementController(const Json & characterMovementControllerJson)
{
	sf::Vector2f characterVelocity(0.0f, 0.0f);
	sf::Vector2f boundariesX(0.0f, 1280.0f);
	sf::Vector2f boundariesY(0.0f, 720.0f);

	if (characterMovementControllerJson.contains("characterVelocity"))
	{
		if (characterMovementControllerJson["characterVelocity"].contains("x"))
		{
			characterVelocity.x = characterMovementControllerJson["characterVelocity"]["x"].get<float>();
		}
		
		if (characterMovementControllerJson["characterVelocity"].contains("y"))
		{
			characterVelocity.y = characterMovementControllerJson["characterVelocity"]["y"].get<float>();

		}
	}

	if (characterMovementControllerJson.contains("boundariesX"))
	{
		if (characterMovementControllerJson["boundariesX"].contains("lowerBound"))
		{
			boundariesX.x = characterMovementControllerJson["boundariesX"]["lowerBound"].get<float>();
		}

		if (characterMovementControllerJson["boundariesX"].contains("upperBound"))
		{
			boundariesX.y = characterMovementControllerJson["boundariesX"]["upperBound"].get<float>();

		}
	}

	if (characterMovementControllerJson.contains("boundariesY"))
	{
		if (characterMovementControllerJson["boundariesY"].contains("lowerBound"))
		{
			boundariesY.x = characterMovementControllerJson["boundariesY"]["lowerBound"].get<float>();
		}

		if (characterMovementControllerJson["boundariesY"].contains("upperBound"))
		{
			boundariesY.y = characterMovementControllerJson["boundariesY"]["upperBound"].get<float>();

		}
	}

	return gameplay::CharacterMovementController(characterVelocity, boundariesX, boundariesY);
}

gameplay::ProjectileShooter gameplay::LevelScene::loadProjectileShooter(const Json & projectileShooterJson)
{
	std::string prefabName = "NONE";
	sf::Time fireRate = sf::seconds(0.5);
	sf::Vector2f boundariesX(0.0f, 1280.0f);
	sf::Vector2f boundariesY(0.0f, 720.0f);
	sf::Vector2f projectileOffset(0.0f, 0.0f);

	if (projectileShooterJson.contains("dataFileLocation"))
	{
		prefabName = projectileShooterJson["dataFileLocation"].get<std::string>();
		loadProjectile(prefabName);
	}

	if (projectileShooterJson.contains("fireRate"))
	{
		fireRate = sf::seconds(projectileShooterJson["fireRate"].get<float>());
	}

	if (projectileShooterJson.contains("boundariesX"))
	{
		if (projectileShooterJson["boundariesX"].contains("lowerBound"))
		{
			boundariesX.x = projectileShooterJson["boundariesX"]["lowerBound"].get<float>();
		}

		if (projectileShooterJson["boundariesX"].contains("upperBound"))
		{
			boundariesX.y = projectileShooterJson["boundariesX"]["upperBound"].get<float>();
		}
	}

	if (projectileShooterJson.contains("boundariesY"))
	{
		if (projectileShooterJson["boundariesY"].contains("lowerBound"))
		{
			boundariesY.x = projectileShooterJson["boundariesY"]["lowerBound"].get<float>();
		}

		if (projectileShooterJson["boundariesY"].contains("upperBound"))
		{
			boundariesY.y = projectileShooterJson["boundariesY"]["upperBound"].get<float>();
		}
	}

	if (projectileShooterJson.contains("projectileOffset"))
	{
		if (projectileShooterJson["projectileOffset"].contains("x"))
		{
			projectileOffset.x = projectileShooterJson["projectileOffset"]["x"].get<float>();
		}

		if (projectileShooterJson["projectileOffset"].contains("y"))
		{
			projectileOffset.y = projectileShooterJson["projectileOffset"]["y"].get<float>();
		}
	}

	return gameplay::ProjectileShooter(prefabName, fireRate, boundariesX, boundariesY, projectileOffset);
}

audio::AudioSource gameplay::LevelScene::loadAudioSource(const Json & audioSourceJson)
{
	audio::AudioSource audioSource;

	if (audioSourceJson.contains("audioClips"))
	{
		for (const Json& soundJson : audioSourceJson["audioClips"])
		{
			if (soundJson.contains("name"))
			{
				audioSource.addAudioClip(soundJson["name"].get<std::string>(), loadSound(soundJson));
			}
		}
	}

	return audioSource;
}

sf::Sound gameplay::LevelScene::loadSound(const Json & soundJson)
{
	sf::Sound sound;
	if (soundJson.contains("fileLocation"))
	{
		std::string fileLocation = soundJson["fileLocation"].get<std::string>();
		loadSoundBuffer(fileLocation);
		sound.setBuffer(*m_soundBufferResourceManager->getResource(fileLocation));
	}

	if (soundJson.contains("volume"))
	{
		sound.setVolume(soundJson["volume"].get<float>());
	}

	return sound;
}

void gameplay::LevelScene::loadSoundBuffer(std::string fileLocation)
{
	sf::SoundBuffer soundBuffer;
	if (!m_soundBufferResourceManager->isResourceLoaded(fileLocation))
	{
		if (soundBuffer.loadFromFile("resources/" + fileLocation))
		{
			m_soundBufferResourceManager->addResource(fileLocation, soundBuffer);
		}
	}
}

graphics::Text gameplay::LevelScene::loadText(const Json & textJson)
{
	graphics::Text text;

	if (textJson.contains("font"))
	{
		std::string fileLocation = textJson["font"].get<std::string>();
		loadFont(fileLocation);
		text.setFont(*(m_fontResourceManager->getResource(fileLocation)));
	}

	if (textJson.contains("characterSize"))
	{
		text.setCharacterSize(textJson["characterSize"].get<int>());
	}

	if (textJson.contains("localPosition"))
	{
		sf::Vector2f localPosition(0.0f, 0.0f);
		if (textJson["localPosition"].contains("x"))
		{
			localPosition.x = textJson["localPosition"]["x"].get<float>();
		}
		if (textJson["localPosition"].contains("y"))
		{
			localPosition.y = textJson["localPosition"]["y"].get<float>();
		}
		text.setLocalPosition(localPosition);
	}

	if (textJson.contains("fillColor"))
	{
		sf::Color color;
		if (textJson["fillColor"].contains("red"))
		{
			color.r = textJson["fillColor"]["red"].get<int>();
		}

		if (textJson["fillColor"].contains("green"))
		{
			color.g = textJson["fillColor"]["green"].get<int>();
		}

		if (textJson["fillColor"].contains("blue"))
		{
			color.b = textJson["fillColor"]["blue"].get<int>();
		}

		if (textJson["fillColor"].contains("alpha"))
		{
			color.a = textJson["fillColor"]["alpha"].get<int>();
		}

		text.setFillColor(color);
	}

	if (textJson.contains("layer"))
	{
		text.setLayer(textJson["layer"].get<int>());
	}

	if (textJson.contains("string"))
	{
		text.setString(textJson["string"].get<std::string>());
	}

	return text;
}

void gameplay::LevelScene::loadFont(std::string fileLocation)
{
	sf::Font font;
	if (!m_fontResourceManager->isResourceLoaded(fileLocation))
	{
		if (font.loadFromFile("resources/" + fileLocation))
		{
			m_fontResourceManager->addResource(fileLocation, font);
		}
	}
}


