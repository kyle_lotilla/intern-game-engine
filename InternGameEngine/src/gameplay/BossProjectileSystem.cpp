#include <gameplay/BossProjectileSystem.h>
#include <physics/Collider.h>
#include <audio/AudioSource.h>

gameplay::BossProjectileSystem::BossProjectileSystem(int bossID, const std::vector<int>& entityIDPool, const std::shared_ptr<ecs::EntityManager>& entitiyManager, const std::shared_ptr<ecs::ComponentManager>& componentManager)
	: m_bossID(bossID), m_entityIDPool(entityIDPool), m_entityManager(entitiyManager), m_componentManager(componentManager)
{
}

void gameplay::BossProjectileSystem::update(sf::Time deltaTime) const
{
	ProjectileShooter* projectileShooter = m_componentManager->getComponent<ProjectileShooter>(m_bossID);
	if (projectileShooter)
	{
		shootProjectile(deltaTime, *projectileShooter);
		checkOutOfBounds(deltaTime, *projectileShooter);
	}
}

void gameplay::BossProjectileSystem::shootProjectile(const sf::Time & deltaTime, ProjectileShooter & projectileShooter) const
{
	if (m_timeSinceLastShot >= projectileShooter.getFireRate())
	{
		if (m_entityIDPool.size() > 0)
		{
			int projectileEntityID = m_entityIDPool.back();
			ecs::Entity* projectileEntity = m_entityManager->getEntity(projectileEntityID);
			ecs::Entity* bossEntity = m_entityManager->getEntity(m_bossID);
			if (projectileEntity && bossEntity)
			{
				m_componentManager->instantiatePrefab(projectileEntityID, projectileShooter.getProjectilePrefabName());
				m_entityIDPool.pop_back();
				m_timeSinceLastShot = sf::Time::Zero;

				sf::Vector2f bossPosition = bossEntity->getTransform().getPosition();
				physics::Transform& projectileTransform = projectileEntity->getTransform();
				sf::Vector2f projectileOffset = projectileShooter.getProjectileOffest();

				projectileTransform.setPosition(bossPosition + projectileOffset);

				m_activeEntityIDPool.push_back(projectileEntityID);
			}

			audio::AudioSource* audioSource = m_componentManager->getComponent<audio::AudioSource>(m_bossID);
			if (audioSource)
			{
				audioSource->playAudioClip("BOSS_SHOOT");
			}
		}
	}
	else
	{
		m_timeSinceLastShot += deltaTime;
	}
}

void gameplay::BossProjectileSystem::checkOutOfBounds(const sf::Time & deltaTime, ProjectileShooter & projectileShooter) const
{
	std::vector<std::vector<int>::iterator> inactiveFlaggedEntities;
	for (auto it = m_activeEntityIDPool.begin(); it != m_activeEntityIDPool.end();)
	{
		int projectileEntityID = *it;
		ecs::Entity* projectileEntity = m_entityManager->getEntity(projectileEntityID);
		if (projectileEntity)
		{
			sf::Vector2f position = projectileEntity->getTransform().getPosition();
			sf::Vector2f boundariesX = projectileShooter.getBoundariesX();
			sf::Vector2f boundariesY = projectileShooter.getBoundariesY();

			if (position.x < boundariesX.x || position.y < boundariesY.x || position.x > boundariesX.y || position.y > boundariesY.y)
			{
				m_componentManager->removeAllComponents(projectileEntityID);
				m_entityIDPool.push_back(projectileEntityID);
				it = m_activeEntityIDPool.erase(it);
			}
			else
			{
				it++;
			}
		}
	}
}
