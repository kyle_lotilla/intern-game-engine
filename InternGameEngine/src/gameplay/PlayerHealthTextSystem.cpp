#include <gameplay/PlayerHealthTextSystem.h>


gameplay::PlayerHealthTextSystem::PlayerHealthTextSystem(int playerID, int playerHealthTextID, const std::shared_ptr<ecs::ComponentManager>& componentManager)
	: m_playerID(playerID), m_playerHealthTextID(playerHealthTextID), m_componentManager(componentManager)
{
}

void gameplay::PlayerHealthTextSystem::start() const
{
	setHealthText();
}

void gameplay::PlayerHealthTextSystem::eventTriggered(events::Event event)
{
	if (event.getEventName() == "PLAYER_DAMAGED")
	{
		setHealthText();
	}
}

void gameplay::PlayerHealthTextSystem::setHealthText() const
{
	Health* health = m_componentManager->getComponent<Health>(m_playerID);
	graphics::Text* text = m_componentManager->getComponent<graphics::Text>(m_playerHealthTextID);

	if (health && text)
	{
		text->setString("X " + std::to_string(health->getHealth()));
	}
}
