#include <gameplay/Health.h>
#include "..\..\include\gameplay\Health.h"

gameplay::Health::Health(int baseHealth)
	: m_baseHealth(baseHealth), m_currrentHealth(baseHealth)
{
}

int gameplay::Health::getHealth()
{
	return m_currrentHealth;
}

void gameplay::Health::damageHealth(int damage)
{
	m_currrentHealth -= damage;
}

float gameplay::Health::getHealthPercent()
{
	return (float)m_currrentHealth / (float)m_baseHealth;
}

bool gameplay::Health::isAlive()
{
	if (m_currrentHealth > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
