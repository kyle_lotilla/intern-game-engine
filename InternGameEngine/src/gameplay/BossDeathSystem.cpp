#include <gameplay/BossDeathSystem.h>
#include <gameplay/Health.h>
#include <gameplay/ProjectileShooter.h>
#include <graphics/AnimationController.h>
#include <physics/Collider.h>
#include <physics/RigidBody2D.h>
#include <audio/AudioSource.h>

gameplay::BossDeathSystem::BossDeathSystem(int bossID, const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<events::EventManager>& eventManager)
	: m_bossID(bossID), m_componentManager(componentManager), m_isDead(false), m_eventManager(eventManager), m_isPlayerDead(false)
{
}

void gameplay::BossDeathSystem::update(sf::Time deltaTime) const
{
	if (!m_isDead)
	{
		gameplay::Health* health = m_componentManager->getComponent<gameplay::Health>(m_bossID);

		if (health)
		{
			if (!health->isAlive())
			{
				graphics::AnimationController* animationController = m_componentManager->getComponent<graphics::AnimationController>(m_bossID);
				audio::AudioSource* audioSource = m_componentManager->getComponent<audio::AudioSource>(m_bossID);


				if (animationController)
				{
					animationController->setColor(sf::Color::White);
					animationController->setCurrentAnimation("BOSS_DEATH");
				}
				
				if (audioSource)
				{
					audioSource->playAudioClip("BOSS_DEATH");
				}

				m_componentManager->removeComponent<physics::Collider>(m_bossID);
				m_componentManager->removeComponent<physics::RigidBody2D>(m_bossID);
				m_componentManager->removeComponent<gameplay::ProjectileShooter>(m_bossID);

				m_isDead = true;

			}
		}
	}
	else
	{
		graphics::AnimationController* animationController = m_componentManager->getComponent<graphics::AnimationController>(m_bossID);
		audio::AudioSource* audioSource = m_componentManager->getComponent<audio::AudioSource>(m_bossID);

		if (animationController && audioSource)
		{
			sf::Sound* soundClip = audioSource->getAudioClip("BOSS_DEATH");
			if (soundClip)
			{
				if (!animationController->isPlaying() && soundClip->getStatus() == sf::Sound::Status::Stopped && !m_isPlayerDead)
				{
					m_componentManager->removeAllComponents(m_bossID);
					m_eventManager->fireEvent("BOSS_DEATH");
				}
			}
		}
	}
}

void gameplay::BossDeathSystem::eventTriggered(events::Event event)
{
	if (event.getEventName() == "PLAYER_DEATH_START")
	{
		m_isPlayerDead = true;
	}
}
