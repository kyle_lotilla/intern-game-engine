#include <gameplay/GameOverMenuSystem.h>
#include <ui/MouseUIButton.h>
#include <engine/EngineRequestComponent.h>

gameplay::GameOverMenuSystem::GameOverMenuSystem(const std::shared_ptr<ecs::ComponentManager>& componentManager)
	: m_componentManager(componentManager), m_isGameOver(true)
{
}

void gameplay::GameOverMenuSystem::update(sf::Time deltaTime) const
{
	if (m_isGameOver)
	{
		const ecs::ComponentArray<ui::MouseUIButton>* mouseUIButtonArray = m_componentManager->getComponentArray<ui::MouseUIButton>();

		if (mouseUIButtonArray)
		{
			for (ui::MouseUIButton& mouseUIButton : *mouseUIButtonArray)
			{
				if (mouseUIButton.isPressed())
				{
					if (mouseUIButton.getButtonName() == "MAIN_MENU")
					{
						const ecs::ComponentArray<engine::EngineRequestComponent>* engineRequestComponentArray = m_componentManager->getComponentArray<engine::EngineRequestComponent>();
						if (engineRequestComponentArray)
						{
							for (engine::EngineRequestComponent& engineRequestComponent : *engineRequestComponentArray)
							{
								engineRequestComponent.engineRequest = "MAIN_MENU";
							}
						}
					}
					else if (mouseUIButton.getButtonName() == "NEXT_LEVEL")
					{
						const ecs::ComponentArray<engine::EngineRequestComponent>* engineRequestComponentArray = m_componentManager->getComponentArray<engine::EngineRequestComponent>();
						if (engineRequestComponentArray)
						{
							for (engine::EngineRequestComponent& engineRequestComponent : *engineRequestComponentArray)
							{
								engineRequestComponent.engineRequest = "NEXT_LEVEL";
							}
						}
					}
					else if (mouseUIButton.getButtonName() == "REPLAY")
					{
						const ecs::ComponentArray<engine::EngineRequestComponent>* engineRequestComponentArray = m_componentManager->getComponentArray<engine::EngineRequestComponent>();
						if (engineRequestComponentArray)
						{
							for (engine::EngineRequestComponent& engineRequestComponent : *engineRequestComponentArray)
							{
								engineRequestComponent.engineRequest = "REPLAY";
							}
						}
					}
				}
			}
		}
	}
}

void gameplay::GameOverMenuSystem::eventTriggered(events::Event event)
{
	if (event.getEventName() == "PLAYER_DEATH" || event.getEventName() == "BOSS_DEATH")
	{
		m_isGameOver = true;
	}
}
