#include <gameplay/BossPhaseSpeedTransitionSystem.h>
#include <gameplay/ProjectileShooter.h>
#include <gameplay/CharacterMovementController.h>
#include <physics/RigidBody2D.h>

gameplay::BossPhaseSpeedTransitionSystem::BossPhaseSpeedTransitionSystem(int bossID, const std::shared_ptr<ecs::ComponentManager>& componentManager)
	: m_bossID(bossID), m_componentManager(componentManager)
{
}

void gameplay::BossPhaseSpeedTransitionSystem::eventTriggered(events::Event event)
{
	if (event.getEventName() == "PHASE_TWO")
	{
		ProjectileShooter* projectileShooter = m_componentManager->getComponent<ProjectileShooter>(m_bossID);
		CharacterMovementController* characterMovementController = m_componentManager->getComponent<CharacterMovementController>(m_bossID);
		physics::RigidBody2D* rigidBody = m_componentManager->getComponent<physics::RigidBody2D>(m_bossID);

		if (projectileShooter && characterMovementController && rigidBody)
		{
			projectileShooter->setFireRate(projectileShooter->getFireRate() * 0.7f);
			characterMovementController->setCharacterVelocity(characterMovementController->getCharacterVelocity() * 1.7f);
			rigidBody->setVelocity(rigidBody->getVelocity() * 1.7f);
		}
	}
}
