#include <gameplay/BossMovementSystem.h>
#include <gameplay/CharacterMovementController.h>
#include <physics/RigidBody2D.h>

gameplay::BossMovementSystem::BossMovementSystem(int bossID, const std::shared_ptr<ecs::EntityManager>& entityManager, const std::shared_ptr<ecs::ComponentManager>& componentManager)
	: m_bossID(bossID), m_entityManager(entityManager), m_componentManager(componentManager)
{
}

void gameplay::BossMovementSystem::start() const
{
	physics::RigidBody2D* rigidBody = m_componentManager->getComponent<physics::RigidBody2D>(m_bossID);
	CharacterMovementController* bossMovementController = m_componentManager->getComponent<CharacterMovementController>(m_bossID);

	if (rigidBody && bossMovementController)
	{
		rigidBody->setVelocity(bossMovementController->getCharacterVelocity());
	}
}

void gameplay::BossMovementSystem::update(sf::Time deltaTime) const
{
	ecs::Entity* entity = m_entityManager->getEntity(m_bossID);
	physics::RigidBody2D* rigidBody = m_componentManager->getComponent<physics::RigidBody2D>(m_bossID);
	CharacterMovementController* bossMovementController = m_componentManager->getComponent<CharacterMovementController>(m_bossID);

	if (entity && rigidBody && bossMovementController)
	{
		sf::Vector2f position = entity->getTransform().getPosition();
		sf::Vector2f bossVeloctiy = bossMovementController->getCharacterVelocity();
		sf::Vector2f boundariesX = bossMovementController->getBoundariesX();

		if (position.x - bossVeloctiy.x * deltaTime.asSeconds() < boundariesX.x)
		{
			rigidBody->setVelocityX(bossVeloctiy.x);
		}
		else if (position.x + bossVeloctiy.x * deltaTime.asSeconds() > boundariesX.y)
		{
			rigidBody->setVelocityX(-bossVeloctiy.x);
		}

	}
}
