#include <gameplay/Damage.h>

gameplay::Damage::Damage(int damage)
	: m_damage(damage)
{
}

int gameplay::Damage::getDamage()
{
	return m_damage;
}
