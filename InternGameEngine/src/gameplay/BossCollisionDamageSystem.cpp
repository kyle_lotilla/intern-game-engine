#include <gameplay/BossCollisionDamageSystem.h>
#include <graphics/AnimationController.h>
#include <physics/Collider.h>
#include <gameplay/Health.h>
#include <gameplay/Damage.h>
#include <audio/AudioSource.h>

gameplay::BossCollisionDamageSystem::BossCollisionDamageSystem(int bossID, const std::shared_ptr<ecs::ComponentManager>& componentManager)
	: m_bossID(bossID), m_componentManager(componentManager), m_neutralColor(sf::Color::White)
{
}

void gameplay::BossCollisionDamageSystem::update(sf::Time deltaTime) const
{
	graphics::AnimationController* animationController = m_componentManager->getComponent<graphics::AnimationController>(m_bossID);
	gameplay::Health* health = m_componentManager->getComponent<gameplay::Health>(m_bossID);
	physics::Collider* collider = m_componentManager->getComponent<physics::Collider>(m_bossID);

	if (animationController && health && collider)
	{
		m_timeSinceLastDamage += deltaTime;

		for (physics::Collision collision : collider->getCollisions())
		{
			if (collision.tagName == "PLAYER_PROJECTILE")
			{
				m_timeSinceLastDamage = sf::Time::Zero;
				animationController->setColor(sf::Color::Red);
				gameplay::Damage* damage = m_componentManager->getComponent<gameplay::Damage>(collision.collidedEntityID);
				if (damage)
				{
					health->damageHealth(damage->getDamage());
				}
				audio::AudioSource* audioSource = m_componentManager->getComponent<audio::AudioSource>(m_bossID);
				if (audioSource)
				{
					audioSource->playAudioClip("BOSS_DAMAGE");
				}
			}
		}

		if (m_timeSinceLastDamage >= DAMAGE_COLOR_TIME)
		{
			animationController->setColor(m_neutralColor);
		}
	}

}

void gameplay::BossCollisionDamageSystem::eventTriggered(events::Event event)
{
	if (event.getEventName() == "PHASE_TWO")
	{
		m_neutralColor = sf::Color(200, 0, 0);
	}
}
