#include <gameplay/PlayerDeathSystem.h>
#include <gameplay/Health.h>
#include <graphics/AnimationController.h>
#include <physics/Collider.h>
#include <physics/RigidBody2D.h>
#include <audio/AudioSource.h>

gameplay::PlayerDeathSystem::PlayerDeathSystem(int playerID, const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<events::EventManager>& eventManager)
	: m_playerID(playerID), m_componentManager(componentManager), m_isDead(false), m_eventManager(eventManager)
{
}

void gameplay::PlayerDeathSystem::update(sf::Time deltaTime) const
{
	if (!m_isDead)
	{
		gameplay::Health* health = m_componentManager->getComponent<gameplay::Health>(m_playerID);

		if (health)
		{
			if (!health->isAlive())
			{
				graphics::AnimationController* animationController = m_componentManager->getComponent<graphics::AnimationController>(m_playerID);
				audio::AudioSource* audioSource = m_componentManager->getComponent<audio::AudioSource>(m_playerID);


				if (animationController)
				{
					animationController->setColor(sf::Color::White);
					animationController->setCurrentAnimation("PLAYER_DEATH");
				}

				if (audioSource)
				{
					audioSource->playAudioClip("PLAYER_DEATH");
				}

				m_componentManager->removeComponent<physics::Collider>(m_playerID);
				m_componentManager->removeComponent<physics::RigidBody2D>(m_playerID);

				m_isDead = true;
				m_eventManager->fireEvent("PLAYER_DEATH_START");
			}
		}
	}
	else
	{
		graphics::AnimationController* animationController = m_componentManager->getComponent<graphics::AnimationController>(m_playerID);
		audio::AudioSource* audioSource = m_componentManager->getComponent<audio::AudioSource>(m_playerID);

		if (animationController && audioSource)
		{
			sf::Sound* soundClip = audioSource->getAudioClip("PLAYER_DEATH");
			if (soundClip)
			{
				if (!animationController->isPlaying() && soundClip->getStatus() == sf::Sound::Status::Stopped)
				{
					m_componentManager->removeAllComponents(m_playerID);
					m_eventManager->fireEvent("PLAYER_DEATH");
				}
			}
		}
	}
}
