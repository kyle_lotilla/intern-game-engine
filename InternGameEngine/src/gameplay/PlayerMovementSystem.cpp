#include <gameplay/PlayerMovementSystem.h>
#include <physics/RigidBody2D.h>
#include <gameplay/CharacterMovementController.h>


gameplay::PlayerMovementSystem::PlayerMovementSystem(int playerID, const std::shared_ptr<ecs::EntityManager>& entityManager, const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<input::KeyboardInputHandler>& keyboardHandler)
	: m_playerID(playerID), m_entityManager(entityManager), m_componentManager(componentManager), m_keyboardHandler(keyboardHandler)
{
}

void gameplay::PlayerMovementSystem::update(sf::Time deltaTime) const
{
	ecs::Entity* entity = m_entityManager->getEntity(m_playerID);
	physics::RigidBody2D* rigidBody = m_componentManager->getComponent<physics::RigidBody2D>(m_playerID);
	CharacterMovementController* playerMovementController = m_componentManager->getComponent<gameplay::CharacterMovementController>(m_playerID);
		
	if (entity && rigidBody && playerMovementController)
	{
		sf::Vector2f position = entity->getTransform().getPosition();
		sf::Vector2f playerVeloctiy = playerMovementController->getCharacterVelocity();
		sf::Vector2f boundariesX = playerMovementController->getBoundariesX();
		sf::Vector2f boundariesY = playerMovementController->getBoundariesY();

		if (m_keyboardHandler->isActionPressed("UP") && position.y - playerVeloctiy.y * deltaTime.asSeconds() > boundariesY.x)
		{
			rigidBody->setVelocityY(-playerVeloctiy.y);
		}
		else if (m_keyboardHandler->isActionPressed("DOWN") && position.y + playerVeloctiy.y * deltaTime.asSeconds() < boundariesY.y)
		{
			rigidBody->setVelocityY(playerVeloctiy.y);
		}
		else
		{
			rigidBody->setVelocityY(0.0f);
		}
		
		if (m_keyboardHandler->isActionPressed("LEFT") && position.x - playerVeloctiy.x * deltaTime.asSeconds() > boundariesX.x)
		{
			rigidBody->setVelocityX(-playerVeloctiy.x);
		}
		else if (m_keyboardHandler->isActionPressed("RIGHT") && position.x + playerVeloctiy.x * deltaTime.asSeconds() < boundariesX.y)
		{
			rigidBody->setVelocityX(playerVeloctiy.x);
		}
		else
		{
			rigidBody->setVelocityX(0.0f);
		}
		
	}
}
