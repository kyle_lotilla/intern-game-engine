#include <gameplay/PlayerCollisionDamageSystem.h>
#include <graphics/AnimationController.h>
#include <gameplay/Health.h>
#include <physics/Collider.h>
#include <gameplay/Damage.h>
#include <audio/AudioSource.h>

gameplay::PlayerCollisionDamageSystem::PlayerCollisionDamageSystem(int playerID, const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<events::EventManager>& eventManager)
	: m_playerID(playerID), m_componentManager(componentManager), m_eventManager(eventManager), m_isInvunrable(false), m_isTransparent(true)
{
}

void gameplay::PlayerCollisionDamageSystem::update(sf::Time deltaTime) const
{
	graphics::AnimationController* animationController = m_componentManager->getComponent<graphics::AnimationController>(m_playerID);
	gameplay::Health* health = m_componentManager->getComponent<gameplay::Health>(m_playerID);
	physics::Collider* collider = m_componentManager->getComponent<physics::Collider>(m_playerID);

	if (animationController && health && collider)
	{
		m_timeSinceLastDamage += deltaTime;

		if (!m_isInvunrable)
		{
			for (physics::Collision collision : collider->getCollisions())
			{
				if (collision.tagName == "BOSS" || collision.tagName == "BOSS_PROJECTILE")
				{
					m_timeSinceLastDamage = sf::Time::Zero;
					m_isInvunrable = true;
					gameplay::Damage* damage = m_componentManager->getComponent<gameplay::Damage>(collision.collidedEntityID);
					if (damage)
					{
						health->damageHealth(damage->getDamage());
						m_eventManager->fireEvent("PLAYER_DAMAGED");
					}

					audio::AudioSource* audioSource = m_componentManager->getComponent<audio::AudioSource>(m_playerID);

					if (audioSource)
					{
						audioSource->playAudioClip("PLAYER_DAMAGE");
					}
				}
			}
		}
		else
		{
			if (m_timeSinceLastDamage >= INVUNRABLE_TIME)
			{
				animationController->setColor(sf::Color::White);
				m_isInvunrable = false;
				m_isTransparent = true;
			}
			else
			{
				if (m_isTransparent)
				{
					animationController->setColor(sf::Color(0,0,0,0));
				}
				else
				{
					animationController->setColor(sf::Color::Red);
				}
				m_isTransparent = !m_isTransparent;

			}
		}
	}
}
