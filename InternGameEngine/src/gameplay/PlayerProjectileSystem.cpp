#include <gameplay/PlayerProjectileSystem.h>
#include <gameplay/ProjectileShooter.h>
#include <physics/Collider.h>
#include <graphics/AnimationController.h>
#include <audio/AudioSource.h>

gameplay::PlayerProjectileSystem::PlayerProjectileSystem(int playerID, const std::vector<int>& entityIDPool, const std::shared_ptr<ecs::EntityManager>& entitiyManager, const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<input::KeyboardInputHandler>& keyboardHandler)
	: m_playerID(playerID), m_entityIDPool(entityIDPool), m_entityManager(entitiyManager), m_componentManager(componentManager), m_keyboardHandler(keyboardHandler), m_timeSinceLastShot(sf::seconds(600.0f))
{
}

void gameplay::PlayerProjectileSystem::update(sf::Time deltaTime) const
{
	ProjectileShooter* projectileShooter = m_componentManager->getComponent<ProjectileShooter>(m_playerID);
	if (projectileShooter)
	{
		shootProjectile(deltaTime, *projectileShooter);
		checkCollisions(deltaTime);
		checkOutOfBounds(deltaTime, *projectileShooter);
	}
	
}

void gameplay::PlayerProjectileSystem::shootProjectile(const sf::Time& deltaTime, ProjectileShooter& projectileShooter) const
{
	if (m_timeSinceLastShot >= projectileShooter.getFireRate())
	{
		if (m_keyboardHandler->isActionPressed("SHOOT") && m_entityIDPool.size() > 0)
		{
			int projectileEntityID = m_entityIDPool.back();
			ecs::Entity* projectileEntity = m_entityManager->getEntity(projectileEntityID);
			ecs::Entity* playerEntity = m_entityManager->getEntity(m_playerID);

			if (projectileEntity && playerEntity)
			{
				m_componentManager->instantiatePrefab(projectileEntityID, projectileShooter.getProjectilePrefabName());
				m_entityIDPool.pop_back();
				m_timeSinceLastShot = sf::Time::Zero;

				sf::Vector2f playerPosition = playerEntity->getTransform().getPosition();
				physics::Transform& projectileTransform = projectileEntity->getTransform();
				sf::Vector2f projectileOffset = projectileShooter.getProjectileOffest();

				projectileTransform.setPosition(playerPosition + projectileOffset);

				m_activeEntityIDPool.push_back(projectileEntityID);

				audio::AudioSource* audioSource = m_componentManager->getComponent<audio::AudioSource>(m_playerID);
				if (audioSource)
				{
					audioSource->playAudioClip("PLAYER_SHOOT");
				}
			}
			
		}
	}
	else
	{
		m_timeSinceLastShot += deltaTime;
	}
}

void gameplay::PlayerProjectileSystem::checkCollisions(const sf::Time & deltaTime) const
{
	for (std::vector<int>::iterator it = m_activeEntityIDPool.begin(); it != m_activeEntityIDPool.end();)
	{
		int projectileEntityID = *it;
		physics::Collider* collider = m_componentManager->getComponent<physics::Collider>(projectileEntityID);
		if (collider)
		{
			bool removeProjectile = false;
			std::vector<physics::Collision> collisions = collider->getCollisions();
			for (auto itx = collisions.begin(); itx != collisions.end() && !removeProjectile; itx++)
			{
				if (itx->tagName == "BOSS")
				{
					removeProjectile = true;
				}
			}

			if (removeProjectile)
			{
				m_componentManager->removeAllComponents(projectileEntityID);
				m_entityIDPool.push_back(projectileEntityID);
				it = m_activeEntityIDPool.erase(it);
			}
			else
			{
				it++;
			}
		}
	}
}

void gameplay::PlayerProjectileSystem::checkOutOfBounds(const sf::Time& deltaTime, ProjectileShooter& projectileShooter) const
{
	std::vector<std::vector<int>::iterator> inactiveFlaggedEntities;
	for (auto it = m_activeEntityIDPool.begin(); it != m_activeEntityIDPool.end();)
	{
		int projectileEntityID = *it;
		ecs::Entity* projectileEntity = m_entityManager->getEntity(projectileEntityID);
		if (projectileEntity)
		{
			sf::Vector2f position = projectileEntity->getTransform().getPosition();
			sf::Vector2f boundariesX = projectileShooter.getBoundariesX();
			sf::Vector2f boundariesY = projectileShooter.getBoundariesY();

			if (position.x < boundariesX.x || position.y < boundariesY.x || position.x > boundariesX.y || position.y > boundariesY.y)
			{
				m_componentManager->removeAllComponents(projectileEntityID);
				m_entityIDPool.push_back(projectileEntityID);
				it = m_activeEntityIDPool.erase(it);
			}
			else
			{
				it++;
			}
		}
	}

}

