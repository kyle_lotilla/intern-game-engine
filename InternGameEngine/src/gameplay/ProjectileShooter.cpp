#include <gameplay/ProjectileShooter.h>

gameplay::ProjectileShooter::ProjectileShooter(std::string projectilePrefabName, const sf::Time & fireRate, const sf::Vector2f & boundariesX, const sf::Vector2f & boundariesY, const sf::Vector2f & projectileOffset)
	: m_projectilePrefabName(projectilePrefabName), m_fireRate(fireRate), m_boundariesX(boundariesX), m_boundariesY(boundariesY), m_projectileOffset(projectileOffset)
{
}

std::string gameplay::ProjectileShooter::getProjectilePrefabName()
{
	return m_projectilePrefabName;
}

sf::Time gameplay::ProjectileShooter::getFireRate()
{
	return m_fireRate;
}

void gameplay::ProjectileShooter::setFireRate(const sf::Time & fireRate)
{
	m_fireRate = fireRate;
}

sf::Vector2f gameplay::ProjectileShooter::getBoundariesX()
{
	return m_boundariesX;
}

void gameplay::ProjectileShooter::setBoundariesX(const sf::Vector2f & boundariesX)
{
	m_boundariesX = boundariesX;
}

sf::Vector2f gameplay::ProjectileShooter::getBoundariesY()
{
	return m_boundariesY;
}

void gameplay::ProjectileShooter::setBoundariesY(const sf::Vector2f & boundariesY)
{
	m_boundariesY = boundariesY;
}

sf::Vector2f gameplay::ProjectileShooter::getProjectileOffest()
{
	return m_projectileOffset;
}

void gameplay::ProjectileShooter::setProjectileOffset(const sf::Vector2f & projectileOffset)
{
	m_projectileOffset = projectileOffset;
}
