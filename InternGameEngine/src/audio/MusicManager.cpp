#include <audio/MusicManager.h>

audio::MusicManager::MusicManager()
	: m_currentMusic("NONE")
{
}

void audio::MusicManager::addMusic(std::string musicName, std::string fileLocation)
{
	if (m_musicMap.find(musicName) == m_musicMap.end())
	{
		m_musicMap.emplace(musicName, std::make_unique<sf::Music>());
		if (!m_musicMap[musicName]->openFromFile(fileLocation))
		{
			m_musicMap.erase(musicName);
		}
	}
}

void audio::MusicManager::playMusic(std::string musicName)
{
	if (m_musicMap.find(musicName) != m_musicMap.end())
	{
		if (m_currentMusic != "NONE")
		{
			m_musicMap[m_currentMusic]->stop();
		}
		m_currentMusic = musicName;
		m_musicMap[musicName]->play();
	}
}

void audio::MusicManager::pauseMusic()
{
	if (m_currentMusic != "NONE")
	{
		m_musicMap[m_currentMusic]->pause();
	}
}

void audio::MusicManager::stopMusic()
{
	if (m_currentMusic != "NONE")
	{
		m_musicMap[m_currentMusic]->stop();
	}
}

sf::Music* audio::MusicManager::getMusic(std::string musicName)
{
	if (m_musicMap.find(musicName) != m_musicMap.end())
	{
		return m_musicMap[musicName].get();
	}
	return nullptr;
}

void audio::MusicManager::clear()
{
	stopMusic();
	m_musicMap.clear();
}


