#include <audio/AudioSource.h>

void audio::AudioSource::addAudioClip(std::string audioClipName, sf::Sound audioClip)
{
	if (m_audioClipMap.find(audioClipName) == m_audioClipMap.end())
	{
		m_audioClipMap.insert({ audioClipName, audioClip });
	}
}

void audio::AudioSource::playAudioClip(std::string audioClipName)
{
	if (m_audioClipMap.find(audioClipName) != m_audioClipMap.end())
	{
		m_audioClipMap[audioClipName].play();
	}
}

void audio::AudioSource::pauseAudioClip(std::string audioClipName)
{
	if (m_audioClipMap.find(audioClipName) != m_audioClipMap.end())
	{
		m_audioClipMap[audioClipName].pause();
	}
}

void audio::AudioSource::stopAudioClip(std::string audioClipName)
{
	if (m_audioClipMap.find(audioClipName) != m_audioClipMap.end())
	{
		m_audioClipMap[audioClipName].stop();
	}
}

sf::Sound* audio::AudioSource::getAudioClip(std::string audioClipName)
{
	if (m_audioClipMap.find(audioClipName) != m_audioClipMap.end())
	{
		return &(m_audioClipMap[audioClipName]);
	}
	return nullptr;
}
