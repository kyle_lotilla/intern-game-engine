#include <input/KeyboardInputHandler.h>
#include <iostream>

input::KeyboardInputHandler::KeyboardInputHandler()
	: m_currentContext("NONE")
{
}

void input::KeyboardInputHandler::pollInput()
{
	if (m_currentContext != "NONE")
	{
		InputContext<sf::Keyboard::Key, ButtonAction>& currentInputContext = m_contextMap.at(m_currentContext);

		for (std::pair<sf::Keyboard::Key, ButtonAction>& mapping : currentInputContext)
		{
			ButtonAction& action = mapping.second;
			if (sf::Keyboard::isKeyPressed(mapping.first))
			{
				action.setIsHeldDown(action.isPressed());
				action.setIsPressed(true);
			}
			else
			{
				action.setIsPressed(false);
			}
		}
	}
}

void input::KeyboardInputHandler::setCurrentContext(std::string contextName)
{
	if (m_contextMap.find(contextName) != m_contextMap.end())
	{
		m_currentContext = contextName;
	}
	else
	{
		m_currentContext = "NONE";
	}
}

void input::KeyboardInputHandler::clear()
{
	m_contextMap.clear();
	m_currentContext = "NONE";
}

void input::KeyboardInputHandler::addInputContext(const InputContext<sf::Keyboard::Key, ButtonAction>& inputContext)
{
	if (m_contextMap.find(inputContext.getContextName()) == m_contextMap.end())
	{
		m_contextMap.insert({ inputContext.getContextName(), inputContext });
	}
}

bool input::KeyboardInputHandler::isActionPressed(std::string actionName)
{
	if (m_currentContext != "NONE")
	{
		InputContext<sf::Keyboard::Key, ButtonAction> currentContext = m_contextMap.at(m_currentContext);
		if (currentContext.isActionInContext(actionName))
		{
			const std::pair<sf::Keyboard::Key, ButtonAction>* mapping = currentContext.getInputActionMapping(actionName);
			return mapping->second.isPressed();
		}
	}
	return false;
}

const input::ButtonAction * input::KeyboardInputHandler::getButtonAction(std::string actionName)
{
	if (m_currentContext != "NONE")
	{
		InputContext<sf::Keyboard::Key, ButtonAction> currentContext = m_contextMap.at(m_currentContext);

		if (currentContext.isActionInContext(actionName))
		{
			const std::pair<sf::Keyboard::Key, ButtonAction>* mapping = currentContext.getInputActionMapping(actionName);
			return &(mapping->second);
		}
	}
	return nullptr;	
}


