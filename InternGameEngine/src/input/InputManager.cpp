#include <input/InputManager.h>

void input::InputManager::pollInput()
{
	for (std::shared_ptr<IInputHandler>& inputHandler : m_inputHandlers)
	{
		inputHandler->pollInput();
	}
}

void input::InputManager::setCurrentContext(std::string contextName)
{
	m_currentContext = contextName;
	for (std::shared_ptr<IInputHandler>& inputHandler : m_inputHandlers)
	{
		inputHandler->setCurrentContext(contextName);
	}
}

void input::InputManager::clear()
{
	m_inputHandlers.clear();
}
