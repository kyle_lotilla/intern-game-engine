#include <input/IAction.h>

input::IAction::IAction(std::string actionName)
	: m_actionName(actionName)
{
}

std::string input::IAction::getActionName() const
{
	return m_actionName;
}
