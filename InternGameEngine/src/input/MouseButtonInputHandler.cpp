#include <input/MouseButtonInputHandler.h>
#include <iostream>

input::MouseButtonInputHandler::MouseButtonInputHandler(const sf::RenderWindow* window)
	: m_currentContext("NONE"),  m_window(window)
{
}

void input::MouseButtonInputHandler::pollInput()
{
	if (m_currentContext != "NONE")
	{
		InputContext<sf::Mouse::Button, MouseButtonAction>& currentInputContext = m_contextMap.at(m_currentContext);

		for (std::pair<sf::Mouse::Button, MouseButtonAction>& mapping : currentInputContext)
		{
			MouseButtonAction& action = mapping.second;
			if (sf::Mouse::isButtonPressed(mapping.first))
			{
				action.setIsHeldDown(action.isPressed());
				action.setIsPressed(true);
				action.setMousePosition(m_window->mapPixelToCoords(sf::Mouse::getPosition(*m_window)));
			}
			else
			{
				action.setIsPressed(false);
			}

		}
	}
}

void input::MouseButtonInputHandler::setCurrentContext(std::string contextName)
{
	if (m_contextMap.find(contextName) != m_contextMap.end())
	{
		m_currentContext = contextName;
	}
	else
	{
		m_currentContext = "NONE";
	}
}

void input::MouseButtonInputHandler::clear()
{
	m_contextMap.clear();
	m_currentContext = "NONE";
}

void input::MouseButtonInputHandler::addInputContext(const InputContext<sf::Mouse::Button, MouseButtonAction>& inputContext)
{
	if (m_contextMap.find(inputContext.getContextName()) == m_contextMap.end())
	{
		m_contextMap.insert({ inputContext.getContextName(), inputContext });
	}
}

bool input::MouseButtonInputHandler::isActionPressed(std::string actionName)
{
	if (m_currentContext != "NONE")
	{
		InputContext<sf::Mouse::Button, MouseButtonAction> currentContext = m_contextMap.at(m_currentContext);

		if (currentContext.isActionInContext(actionName))
		{
			const std::pair<sf::Mouse::Button, MouseButtonAction>* mapping = currentContext.getInputActionMapping(actionName);
			return mapping->second.isPressed();
		}
	}
	return false;
}

sf::Vector2f input::MouseButtonInputHandler::getMousePosition(std::string actionName)
{
	if (m_currentContext != "NONE")
	{
		InputContext<sf::Mouse::Button, MouseButtonAction> currentContext = m_contextMap.at(m_currentContext);

		if (currentContext.isActionInContext(actionName))
		{
			const std::pair<sf::Mouse::Button, MouseButtonAction>* mapping = currentContext.getInputActionMapping(actionName);
			return mapping->second.getMousePosition();
		}
	}
	return sf::Vector2f(-1.0f, -1.0f);
}

