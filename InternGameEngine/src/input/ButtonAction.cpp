#include <input/ButtonAction.h>

input::ButtonAction::ButtonAction(std::string actionName)
	: IAction(actionName), m_isPressed(false), m_isHeldDown(false)
{
}

bool input::ButtonAction::isPressed() const
{
	return m_isPressed;
}

void input::ButtonAction::setIsPressed(bool isPressed)
{
	m_isPressed = isPressed;
}

bool input::ButtonAction::isHeldDown() const
{
	return m_isHeldDown;
}

void input::ButtonAction::setIsHeldDown(bool isHeldDown)
{
	m_isHeldDown = isHeldDown;
}
