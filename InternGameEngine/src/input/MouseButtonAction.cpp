#include <input/MouseButtonAction.h>

input::MouseButtonAction::MouseButtonAction(std::string actionName)
	: IAction(actionName), m_mousePosition(sf::Vector2f(0.0f,0.0f))
{
}

bool input::MouseButtonAction::isPressed() const
{
	return m_isPressed;
}

void input::MouseButtonAction::setIsPressed(bool isPressed)
{
	m_isPressed = isPressed;
}

bool input::MouseButtonAction::isHeldDown() const
{
	return m_isHeldDown;
}

void input::MouseButtonAction::setIsHeldDown(bool isHeldDown)
{
	m_isHeldDown = isHeldDown;
}

sf::Vector2f input::MouseButtonAction::getMousePosition() const
{
	return m_mousePosition;
}

void input::MouseButtonAction::setMousePosition(const sf::Vector2f& mousePosition)
{
	m_mousePosition = mousePosition;
}


