#include <ui/MouseUIButtonSystem.h>
#include <ui/MouseUIButton.h>
#include <input/MouseButtonAction.h>

ui::MouseUIButtonSystem::MouseUIButtonSystem(const std::shared_ptr<ecs::EntityManager>& entityManager, const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<input::MouseButtonInputHandler>& mouseButtonInputHandler)
	: m_entityManager(entityManager), m_componentManager(componentManager), m_mouseButtonInputHandler(mouseButtonInputHandler)
{
}

void ui::MouseUIButtonSystem::update(sf::Time deltaTime) const
{
	const ecs::ComponentArray<MouseUIButton>* mouseUIButtonArray = m_componentManager->getComponentArray<MouseUIButton>();
	if (mouseUIButtonArray)
	{
		for (MouseUIButton& mouseUIButton : *mouseUIButtonArray)
		{
			mouseUIButton.setPressed(false);
		}

		if (m_mouseButtonInputHandler->isActionPressed("LEFT_CLICK"))
		{
			sf::Vector2f mousePosition = m_mouseButtonInputHandler->getMousePosition("LEFT_CLICK");
			bool isButtonPressed = false;

			for (auto mouseUIButton = mouseUIButtonArray->begin(); mouseUIButton < mouseUIButtonArray->end() && !isButtonPressed; mouseUIButton++)
			{
				ecs::Entity* mouseUIButtonEntity = m_entityManager->getEntity(mouseUIButton->getEntityID());
				if (mouseUIButtonEntity)
				{
					physics::BoundingBox& boundingBox = mouseUIButton->getBoundingBox();
					sf::Vector2f mouseUIButtonPosition = mouseUIButtonEntity->getTransform().getPosition();
					boundingBox.left = mouseUIButtonPosition.x + boundingBox.localLeft;
					boundingBox.top = mouseUIButtonPosition.y + boundingBox.localTop;

					if (boundingBox.contains(mousePosition))
					{
						mouseUIButton->setPressed(true);
						isButtonPressed = true;
					}
				}
			}
		}
	}
	
}
