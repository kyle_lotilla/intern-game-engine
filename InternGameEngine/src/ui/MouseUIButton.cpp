#include <ui/MouseUIButton.h>

ui::MouseUIButton::MouseUIButton(std::string buttonName, const physics::BoundingBox & boundingBox)
	: m_buttonName(buttonName), m_boundingBox(boundingBox), m_isPressed(false)
{
}

std::string ui::MouseUIButton::getButtonName()
{
	return m_buttonName;
}

physics::BoundingBox & ui::MouseUIButton::getBoundingBox()
{
	return m_boundingBox;
}

bool ui::MouseUIButton::isPressed()
{
	return m_isPressed;
}

void ui::MouseUIButton::setPressed(bool pressed)
{
	m_isPressed = pressed;
}
