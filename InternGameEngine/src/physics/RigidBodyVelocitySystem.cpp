#include <physics/RigidBodyVelocitySystem.h>

physics::RigidBodyVelocitySystem::RigidBodyVelocitySystem(const std::shared_ptr<ecs::ComponentManager>& componentManager, const std::shared_ptr<ecs::EntityManager>& entityManager)
	: m_componentManager(componentManager), m_entityManager(entityManager)
{
}

void physics::RigidBodyVelocitySystem::update(sf::Time deltaTime) const
{
	const ecs::ComponentArray<RigidBody2D>* rigidBodyArray = m_componentManager->getComponentArray<RigidBody2D>();

	if (rigidBodyArray)
	{
		for (RigidBody2D& rigidBody : *rigidBodyArray)
		{
			ecs::Entity* entity = m_entityManager->getEntity(rigidBody.getEntityID());
			if (entity)
			{
				physics::Transform& transform = entity->getTransform();
				sf::Vector2f& velocity = rigidBody.getVelocity();
				//std::cout << transform.getPosition().x << ", " << transform.getPosition().y << std::endl;
				transform.setPosition(transform.getPosition() + velocity * deltaTime.asSeconds());
			}
		}
	}
}
