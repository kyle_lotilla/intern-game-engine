#include <physics/BoundingBox.h>


physics::BoundingBox::BoundingBox(const sf::Vector2f & localPosition, const sf::Vector2f & size)
	: sf::FloatRect(0.0f, 0.0f, size.x, size.y), localLeft(localPosition.x), localTop(localPosition.y)
{
}

physics::BoundingBox::BoundingBox(float localLeft_, float localTop_, float width, float height)
	: sf::FloatRect(0.0f, 0.0f, width, height), localLeft(localLeft_), localTop(localTop_)
{
}
