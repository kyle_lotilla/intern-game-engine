#include <physics/CollisionSystem.h>
#include <physics/Collider.h>

physics::CollisionSystem::CollisionSystem(std::shared_ptr<ecs::ComponentManager> componentManager)
	: m_componentManager(componentManager)
{
}

void physics::CollisionSystem::update(sf::Time deltaTime) const
{
	const ecs::ComponentArray<Collider>* colliderArray = m_componentManager->getComponentArray<Collider>();

	if (colliderArray)
	{
		for (Collider& collider : *colliderArray)
		{
			collider.clearCollisions();
		}

		for (auto collider = colliderArray->begin(); collider < colliderArray->end(); collider++)
		{
			for (auto otherCollider = collider + 1; otherCollider < colliderArray->end(); otherCollider++)
			{
				collider->checkCollided(*otherCollider);
			}
		}
	}
	
}
