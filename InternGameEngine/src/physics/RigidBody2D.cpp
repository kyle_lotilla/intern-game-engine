#include <physics/RigidBody2D.h>

physics::RigidBody2D::RigidBody2D()
	: m_velocity(sf::Vector2f(0.0f, 0.0f))
{
}

sf::Vector2f& physics::RigidBody2D::getVelocity()
{
	return m_velocity;
}

void physics::RigidBody2D::setVelocity(const sf::Vector2f& velocity)
{
	m_velocity = velocity;
}

void physics::RigidBody2D::setVelocityX(float xVelocity)
{
	m_velocity.x = xVelocity;
}

void physics::RigidBody2D::setVelocityY(float yVelocity)
{
	m_velocity.y = yVelocity;
}
