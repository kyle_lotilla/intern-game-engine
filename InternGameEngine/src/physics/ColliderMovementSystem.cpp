#include <physics/ColliderMovementSystem.h>
#include <physics/Collider.h>

physics::ColliderMovementSystem::ColliderMovementSystem(std::shared_ptr<ecs::EntityManager> entityManager, std::shared_ptr<ecs::ComponentManager> componentManager)
	: m_entityManager(entityManager), m_componentManager(componentManager)
{
}

void physics::ColliderMovementSystem::update(sf::Time deltaTime) const
{
	const ecs::ComponentArray<Collider>* colliderArray = m_componentManager->getComponentArray<Collider>();

	if (colliderArray)
	{
		for (Collider& collider : *colliderArray)
		{
			ecs::Entity* entity = m_entityManager->getEntity(collider.getEntityID());
			if (entity)
			{
				Transform& transform = entity->getTransform();
				collider.setPosition(transform.getPosition());
			}
		}
	}
}
