#include <physics/Collider.h>
#include <iostream>

physics::Collider::Collider(std::string tagName)
	: m_tagName(tagName)
{
}

const std::string& physics::Collider::getTagName()
{
	return m_tagName;
}

const std::vector<physics::BoundingBox>& physics::Collider::getBoundingBoxes()
{
	return m_boundingBoxes;
}


const std::vector<physics::Collision>& physics::Collider::getCollisions()
{
	return m_collisions;
}

void physics::Collider::addBoundingBox(const BoundingBox& boundingBox)
{
	m_boundingBoxes.push_back(boundingBox);
}

void physics::Collider::setPosition(sf::Vector2f position)
{
	for (BoundingBox& boundingBox : m_boundingBoxes)
	{
		boundingBox.left = position.x + boundingBox.localLeft;
		boundingBox.top = position.y + boundingBox.localTop;
	}
}

void physics::Collider::addCollision(const Collision& collision)
{
	m_collisions.push_back(collision);
}

void physics::Collider::checkCollided(Collider& otherCollider)
{
	const std::vector<physics::BoundingBox>& otherBoundingBoxes = otherCollider.getBoundingBoxes();
	bool isCollided = false;

	for (auto boundingBox = m_boundingBoxes.begin(); boundingBox < m_boundingBoxes.end() && !isCollided; boundingBox++)
	{
		for (auto otherBoundingBox = otherBoundingBoxes.begin(); otherBoundingBox < otherBoundingBoxes.end() && !isCollided; otherBoundingBox++)
		{
			if (boundingBox->intersects(*otherBoundingBox))
			{
				this->addCollision({ otherCollider.getTagName(), otherCollider.getEntityID() });
				otherCollider.addCollision({ m_tagName, this->getEntityID() });
				isCollided = true;
			}
		}
	}
	
}

void physics::Collider::clearCollisions()
{
	m_collisions.clear();
}
