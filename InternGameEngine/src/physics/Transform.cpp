#include <physics/Transform.h>

physics::Transform::Transform(const sf::Vector2f & position)
	: m_position(position), m_rotation(0), m_scale(sf::Vector2f(1.0f, 1.0f))
{
}

physics::Transform::Transform(const sf::Vector2f & position, float rotation, const sf::Vector2f & scale)
	: m_position(position), m_rotation(rotation), m_scale(scale)
{
}

sf::Vector2f& physics::Transform::getPosition()
{
	return m_position;
}

void physics::Transform::setPosition(const sf::Vector2f& position)
{
	m_position = position;
}

float physics::Transform::getRotation()
{
	return m_rotation;
}

void physics::Transform::setRotation(float rotation)
{
	m_rotation = rotation;
}

sf::Vector2f& physics::Transform::getScale()
{
	return m_scale;
}

void physics::Transform::setScale(const sf::Vector2f & scale)
{
	m_scale = scale;
}

