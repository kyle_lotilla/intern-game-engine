#include <engine/GameApp.h>
#include <stdexcept>
#include <iostream>

engine::GameApp::GameApp()
	: m_renderWindow(sf::VideoMode(1280, 720), "Game Application"), m_isRunning(false), m_currentScene(nullptr)
{
}

engine::GameApp::GameApp(int width, int height, const std::string& title)
	: m_renderWindow(sf::VideoMode(width, height), title), m_isRunning(false), m_currentScene(nullptr)
{
}

void engine::GameApp::run()
{
	initialize();

	try
	{
		if (m_currentScene)
		{
			m_isRunning = true;
			gameLoop();
		}
		else
		{
			throw std::logic_error("NO SCENE LOADED");
		}
	}
	catch (const std::logic_error& e)
	{
		std::cout << e.what() << std::endl;
	}
	
	
}

void engine::GameApp::stop()
{
	m_currentScene->unloadScene();
	m_isRunning = false;
}

sf::RenderWindow& engine::GameApp::getWindow()
{
	return m_renderWindow;
}



void engine::GameApp::loadScene(std::string sceneName, util::ArgsObject loadArgs)
{
	try
	{
		if (m_sceneMap.find(sceneName) != m_sceneMap.end())
		{
			m_renderWindow.clear(sf::Color::Black);
			if (m_currentScene)
			{
				m_currentScene->unloadScene();
			}
			m_currentScene = m_sceneMap[sceneName].get();
			m_currentScene->loadScene(loadArgs);
		}
		else
		{
			throw std::out_of_range("SCENE NOT FOUND");
		}
	}
	catch (const std::out_of_range& e)
	{
		std::cout << e.what() << std::endl;
	}
}

void engine::GameApp::processEvents()
{
	m_currentScene->pollInput();
}

void engine::GameApp::processEngineRequest()
{
	std::string engineRequest = m_currentScene->getEngineRequest();
	util::ArgsObject engineReuqestArgs = m_currentScene->getEngineRequestArgs();
	m_currentScene->clearRequest();

	if (engineRequest == "LOAD_SCENE")
	{
		loadScene(*(engineReuqestArgs.getStringArg("SCENE_NAME")), engineReuqestArgs);
		m_clock.restart();
	}
	else if (engineRequest == "QUIT_GAME")
	{
		stop();
	}
}

void engine::GameApp::update(const sf::Time& deltaTime)
{
	m_currentScene->update(deltaTime);
}

void engine::GameApp::render(float interpolation)
{
	m_renderWindow.clear();
	m_currentScene->render(interpolation, m_renderWindow);
	m_renderWindow.display();
}

void engine::GameApp::gameLoop()
{
	sf::Time elapsedTime = sf::Time::Zero;
	sf::Time accumulator = sf::Time::Zero;
	sf::Event event;
	std::string engineRequest;

	while (m_isRunning  && m_renderWindow.isOpen())
	{
		elapsedTime = m_clock.restart();
		accumulator += elapsedTime;

		if (m_renderWindow.pollEvent(event)) {
			if (event.type == sf::Event::Closed)
			{
				m_renderWindow.close();
				stop();
			}
		}

		if (m_isRunning)
		{
			while (accumulator >= TIME_PER_UPDATE)
			{
				accumulator -= TIME_PER_UPDATE;
				processEvents();
				update(TIME_PER_UPDATE);
			}

			float interpolation = accumulator / TIME_PER_UPDATE;
			render(interpolation);

			processEngineRequest();
		}
		
	}
}
