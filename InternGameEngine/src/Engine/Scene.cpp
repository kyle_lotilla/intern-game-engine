#include <engine/Scene.h>

engine::Scene::Scene()
	: m_engineRequest("NONE")
{
}

void engine::Scene::requestLoadScene(std::string sceneName, util::ArgsObject loadArgs)
{
	m_engineRequest = "LOAD_SCENE";
	m_engineRequestArgs = loadArgs;
	m_engineRequestArgs.setStringArg("SCENE_NAME", sceneName);
}

void engine::Scene::requestQuitGame(util::ArgsObject quitArgs)
{
	m_engineRequest = "QUIT_GAME";
	m_engineRequestArgs = quitArgs;
}

std::string engine::Scene::getEngineRequest()
{
	return m_engineRequest;
}

const util::ArgsObject & engine::Scene::getEngineRequestArgs()
{
	return m_engineRequestArgs;
}

void engine::Scene::clearRequest()
{
	m_engineRequest = "NONE";
	m_engineRequestArgs = util::ArgsObject();
}
