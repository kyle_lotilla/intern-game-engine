#include <iostream>
#include <ecs/IComponent.h>

int ecs::IComponent::getEntityID()
{
	return m_entityID;
}

void ecs::IComponent::setEntityID(int entityID)
{
	if (m_entityID < 0)
	{
		m_entityID = entityID;
	}
}

ecs::IComponent::IComponent()
	: m_entityID(-1)
{
}
