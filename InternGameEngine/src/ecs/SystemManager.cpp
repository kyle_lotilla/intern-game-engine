#include <ecs/SystemManager.h>
#include <iostream>

void ecs::SystemManager::start()
{
	for (std::shared_ptr<ISystem>& system : m_systems)
	{
		system->start();
	}
}

void ecs::SystemManager::update(const sf::Time& deltaTime)
{
	for (std::shared_ptr<ISystem>& system : m_systems)
	{
		system->update(deltaTime);
	}
}

void ecs::SystemManager::clear()
{
	m_systems.clear();
}
