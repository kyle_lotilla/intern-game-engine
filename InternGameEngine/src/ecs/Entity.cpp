#include <ecs/Entity.h>

ecs::Entity::Entity(int id)
	: m_ID(id), m_transform(physics::Transform(sf::Vector2f(0.0f, 0.0f)))
{
}

ecs::Entity::Entity(int id, const physics::Transform& transform)
	: m_ID(id), m_transform(transform)
{
}

int ecs::Entity::getID() const
{
	return m_ID;
}

physics::Transform& ecs::Entity::getTransform()
{
	return m_transform;
}

void ecs::Entity::setTransfrom(const physics::Transform& transform)
{
	m_transform = transform;
}
