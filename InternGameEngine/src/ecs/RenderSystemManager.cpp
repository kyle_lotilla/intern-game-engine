#include <ecs/RenderSystemManager.h>
#include <graphics/GraphicsContants.h>
#include <iostream>

void ecs::RenderSystemManager::render(float interpolation, sf::RenderTarget& renderTarget)
{
	for (int i = 0; i < graphics::MAX_LAYERS; i++)
	{
		for (std::shared_ptr<IRenderSystem>& renderSystem : m_renderSystems)
		{
			renderSystem->render(interpolation, renderTarget, i);
		}
	}
}

void ecs::RenderSystemManager::clear()
{
	m_renderSystems.clear();
}
