#include <ecs/EntityManager.h>
#include <ecs/ComponentManager.h>
#include <ecs/ECSConstants.h>

#include <queue>

ecs::EntityManager::EntityManager(std::shared_ptr<ComponentManager> componentManager)
	: m_availableIDs(std::queue<int>()), m_activeEntityCount(0), m_componentManager(componentManager)
{
	for (int i = 0; i < ecs::MAX_ENTITIES; i++)
	{
		m_availableIDs.push(i);
		m_sparseArray[i] = -1;
	}
}

int ecs::EntityManager::createEntity()
{
	m_activeEntityCount++;
	int entityID = m_availableIDs.front();
	m_entities.push_back(Entity(entityID));
	m_sparseArray[entityID] = m_entities.size() - 1;
	m_availableIDs.pop();
	return entityID;
}

int ecs::EntityManager::createEntity(std::string prefabName)
{
	int newEntityID = createEntity();
	m_componentManager->instantiatePrefab(newEntityID, prefabName);
	return newEntityID;
}

int ecs::EntityManager::createEntity(const physics::Transform& transform)
{
	m_activeEntityCount++;
	int entityID = m_availableIDs.front();
	m_entities.push_back(Entity(entityID, transform));
	m_sparseArray[entityID] = m_entities.size() - 1;
	m_availableIDs.pop();
	return entityID;
}

int ecs::EntityManager::createEntity(std::string prefabName, const physics::Transform& transform)
{
	int newEntityID = createEntity(transform);
	m_componentManager->instantiatePrefab(newEntityID, prefabName);
	return newEntityID;
}

std::vector<int> ecs::EntityManager::createEntities(std::size_t numEntities)
{
	std::vector<int> entities;
	for (int i = 0; i < numEntities; i++)
	{
		entities.push_back(createEntity());
	}
	return entities;
}

std::vector<int> ecs::EntityManager::createEntities(std::string prefabName, std::size_t numEntities)
{
	std::vector<int> entities;
	for (int i = 0; i < numEntities; i++)
	{
		entities.push_back(createEntity(prefabName));
	}
	return entities;
}

ecs::Entity* ecs::EntityManager::getEntity(int entityID)
{
	if (m_sparseArray[entityID] < 0)
	{
		return nullptr;
	}
	else
	{
		return &(m_entities[m_sparseArray[entityID]]);
	}
	
}

void ecs::EntityManager::destroyEntity(int entityID)
{
	Entity lastEntity = m_entities.back();
	int lastEntityID = lastEntity.getID();

	m_entities[m_sparseArray[entityID]] = lastEntity;
	m_sparseArray[lastEntityID] = m_sparseArray[entityID];

	m_sparseArray[entityID] = -1;
	m_entities.pop_back();

	m_componentManager->removeAllComponents(entityID);
	m_availableIDs.push(entityID);
	m_activeEntityCount--;
}

