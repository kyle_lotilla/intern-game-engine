#include <events/EventManager.h>

void events::EventManager::addEvent(std::string eventName)
{
	m_eventListenerMap.insert({ eventName, std::vector<std::shared_ptr<IEventListener>>() });
}

void events::EventManager::fireEvent(std::string eventName)
{
	if (m_eventListenerMap.find(eventName) != m_eventListenerMap.end())
	{
		std::vector<std::shared_ptr<IEventListener>>& eventListeners = m_eventListenerMap[eventName];
		
		for (std::shared_ptr<IEventListener> eventListener : eventListeners)
		{
			Event event(eventName);
			eventListener->eventTriggered(event);
		}
	}
}

void events::EventManager::fireEvent(Event event)
{
	if (m_eventListenerMap.find(event.getEventName()) != m_eventListenerMap.end())
	{
		std::vector<std::shared_ptr<IEventListener>>& eventListeners = m_eventListenerMap[event.getEventName()];

		for (std::shared_ptr<IEventListener> eventListener : eventListeners)
		{
			eventListener->eventTriggered(event);
		}
	}
}

void events::EventManager::clear()
{
	m_eventListenerMap.clear();
}
