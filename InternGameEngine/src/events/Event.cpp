#include <events/Event.h>

events::Event::Event(std::string eventName)
	: m_eventName(eventName)
{
}

std::string events::Event::getEventName() const
{
	return m_eventName;
}

