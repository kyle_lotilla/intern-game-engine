#include <util/ArgsObject.h>

const int* util::ArgsObject::getIntArg(std::string argName) const
{
	if (m_intArgsMap.find(argName) != m_intArgsMap.end())
	{
		return &(m_intArgsMap.at(argName));
	}
	return nullptr;
}

void util::ArgsObject::setIntArg(std::string argName, int intArg)
{
	if (m_intArgsMap.find(argName) == m_intArgsMap.end())
	{
		m_intArgsMap.insert({ argName, intArg });
	}
}

const float* util::ArgsObject::getFloatArg(std::string argName) const
{
	if (m_floatArgsMap.find(argName) != m_floatArgsMap.end())
	{
		return &(m_floatArgsMap.at(argName));
	}
	return nullptr;
}

void util::ArgsObject::setFloatArg(std::string argName, float floatArg)
{
	if (m_floatArgsMap.find(argName) == m_floatArgsMap.end())
	{
		m_floatArgsMap.insert({ argName, floatArg });
	}
}

const bool* util::ArgsObject::getBoolArg(std::string argName) const
{
	if (m_boolArgsMap.find(argName) != m_boolArgsMap.end())
	{
		return &(m_boolArgsMap.at(argName));
	}
	return nullptr;
}

void util::ArgsObject::setBoolArg(std::string argName, bool boolArg)
{
	if (m_boolArgsMap.find(argName) == m_boolArgsMap.end())
	{
		m_boolArgsMap.insert({ argName, boolArg });
	}
}

const std::string* util::ArgsObject::getStringArg(std::string argName) const
{
	if (m_stringArgsMap.find(argName) != m_stringArgsMap.end())
	{
		return &(m_stringArgsMap.at(argName));
	}
	return nullptr;
}

void util::ArgsObject::setStringArg(std::string argName, std::string stringArg)
{
	if (m_stringArgsMap.find(argName) == m_stringArgsMap.end())
	{
		m_stringArgsMap.insert({ argName, stringArg });
	}
}
